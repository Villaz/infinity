///<reference path='../typings/node/node.d.ts' />
var fs = require("fs")
var Promise = require("bluebird")
var nconf = require("nconf");
var cluster = require('cluster');
var restify = require('restify');
var winston = require('winston');
var program = require('commander');
var MongoClient = require('mongodb').MongoClient;
var Dashboard = require('./dashboard/dashboard').Dashboard


nconf.file({ file: './infinity.conf' });
var galaxies = nconf.get("universe")["galaxies"]

winston.cli()
var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({ level: 'debug' }),
        new (winston.transports.DailyRotateFile)({ level: 'debug', filename: 'server.log' })
    ]
});

program
    .version('0.0.1')
    .option('-w, --worker <n>', 'Set worker node', parseInt)
    .option('-d, --delete [site]', 'Delete all vms from site or from all sites if it is not defined','none')
    .parse(process.argv);

var workers = {}
var clients = []
var db = undefined


if (cluster.isMaster) {
     master();

} else {
  if(process.env.dashboard !== undefined)
    dashboard()
  else
    worker()
}

function delete_vms() {
    var galax = Object.keys(galaxies)
    var promises = []
    for(var i in galax){
        var infinity = new (require("./infinity")).Infinity(galax[i]);
        promises.push(infinity.deleteAll());
    }
    Promise.all(promises).catch(function (err) { logger.error(err);}).finally(function () { logger.info("Program ended"); process.exit() });
}

function master() {
    cluster.setupMaster({
        exec: 'src/app.js',
        args: ['RemoteDebug.js']
    });
    var galax = Object.keys(galaxies)
    if(program.delete !== 'none'){
      var infinity = new (require("./infinity")).Infinity(program.delete)
      infinity.deleteAll().then(()=>{process.exit()})
    }else{
      var creation = 5
      if(program.worker)
        creation = program.worker
        for (var i in galax) {
          var worker = cluster.fork({ key: galax[i] , creation: creation });
          workers[worker.process.pid] = {site:galax[i]}
        //workers[galax[i]].on('message',processForkMessages)
        //var infinity = new (require("./infinity")).Infinity(galax[i]);
        //infinity.start()
      }
      var dashboard = cluster.fork({dashboard:true})

      cluster.on('exit', function(worker, code, signal) {
        var site = workers[worker.process.pid].site
        var nworker = cluster.fork({ key: site , creation: creation });
        delete workers[worker.process.pid]
        workers[nworker.process.pid] = {site:site}
      })
    }



    function processRequest(req, res, next) {
        if (req.params.id == undefined || req.params.time == undefined || req.params.site == undefined) {
            res.send(400, "Check the params {id,time,site}")
                return next();
        }
        var type = req.path().substr(1).toLowerCase();
        logger.debug("Received HTTP " + type);
        var message = {
            "type": type,
            "body": {
                id: req.params.id,
                time: req.params.time
            }
        }
        try {
            updateDB(req.params.id, type, req.params.time).then(()=>{
              workers[req.params.site].send(JSON.stringify(message));
              res.send(201);
            });
        } catch (e) {
            logger.error(type.toUpperCase() +" "+ req.params.site + " not exists in server")
            res.send(500);
        }
        return next();
    }

    function getCode(request, response, next) {
        var build = fs.readFileSync("build.zip");
        response.header('Content-Length', build.length);
        response.header('Content-Type', 'application/octet-stream');
        response.header('Content-Disposition', 'attachment;filename=build.zip');
        response.send(200, build);
        return next;
    }

    function getStartData(request, response, next) {
        var file = fs.readFileSync(request.files.file.path);
        fs.writeFileSync(request.params.id+".txt", file);
        response.send("100-continue");
        return next;
    }

    function getStartError(request, response, next) {
        var file = fs.readFileSync(request.files.file.path);
        fs.writeFileSync(request.params.id + "_error.txt", file);
        response.send("100-continue");
        return next;
    }

    function updateDB(id:string, type:string, time:string){
        var document = {};
        document[type] = time;
        var collection = db.collection('servers');
        return new Promise((resolve,reject) => {
        collection.update({ 'hostname': id },
            { $set: document }, (err, doc )=>{
                resolve();
            });
        });

    }

    var url = 'mongodb://luis:Espronceda@ds047911.mongolab.com:47911/infinity';
    MongoClient.connect(url, function(err, mongo){
        db = mongo;
    });

    var server = restify.createServer()
    server.pre(restify.pre.userAgentConnection())
    server.use(restify.bodyParser())
    server.put('/heartbeat', processRequest)
    server.put('/boot', processRequest)
    server.put('/dependencies', processRequest)
    server.put('/end', processRequest)
    server.put('/start', processRequest)
    server.get('/code', getCode)
    server.post('start_data', getStartData)
    server.post('start_error', getStartError)

    server.listen(8080, function () {
        console.log('%s listening at %s', server.name, server.url);
    });


    fs.watch('infinity.json', function (event, filename) {
        console.log('event is: ' + event);
        if (filename && event == 'change') {
            console.log('filename provided: ' + filename);
            var work = Object.keys(workers)
            for (var i in work) {
                var message = { "type": "conf" }
                workers[i].send(JSON.stringify(message));
            }
        }
    });
}

function dashboard(){
  var dashboard = new Dashboard(galaxies)
}


function worker() {
    console.log("Cluster " + process.env.key);
    var infinity = new (require("./infinity")).Infinity(process.env.key, process.env.creation);
    infinity.start()
}
