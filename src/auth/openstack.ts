///<reference path='../../typings/node/node.d.ts' />
var fs = require('fs')
var Promise = require("bluebird")
var http = require("../utils/http")

export class Openstack {
    url: string
    cert: string


    constructor(url: string, cert: string= undefined) {
        if (url.indexOf("v2.0") > 0)
            url = url.substr(0, url.lastIndexOf("v2.0")-1);
        this.url = url
        this.cert = cert
    }

    auth(): any {
        var obj = this
        return new Promise(function (resolve, reject) {
            obj.authenticate().then(function (token) {
                obj.getTenants(token).then(function (tenants) {
                    obj.authenticateVOs(tenants, token).then(function (token) {
                        resolve(token);
                    });
                });
            });
        });
    }

    authenticate( ): any {
        var url = this.url + "/v2.0/tokens"
        var cert = this.cert
        return new Promise(function (resolve, reject) {
            try {
                cert = fs.readFileSync(cert)
            } catch (e) {
                if (e.message.indexOf("ENOENT") >= 0) {
                    reject("The file " + cert + " doesn't exists");
                } else {
                    reject(e)
                }
                return
            }

            var data = { "auth": { "voms": true } }
            var header = { "Content-type": "application/json", "Content-Length": JSON.stringify(data).length }
            http.Post(url, data, header, cert).then(function (result) {
                var result = JSON.parse(result.body.toString())
                resolve(result.access.token.id)
            });
        });
    }

    _authenticateVO(vo: string, token: string): any {
        var url = this.url + "/v2.0/tokens"
        var file = this.cert
        var data = { "auth": { "voms": true, "tenantName": vo } }
        var headers = {
            "Accept": "application/json",
            "X-Auth-Token": token,
            "Content-Type": "application/json",
            "Content-Length": JSON.stringify(data).length
        }

        return new Promise(function (resolve, reject) {
            http.Post(url, data, headers, fs.readFileSync(file)).then(function (result) {
                var result = JSON.parse(result.body.toString())
                if (result.access)
                    resolve(result.access.token.id)
                else
                    reject(result.error.title)
            });
        });
    }

    authenticateVOs(vos: Array<string>, token: string): any {
        var obj = this;
        return obj._authenticateVO(vos.pop(), token).then((result)=>{
            return Promise.resolve(result)
        }).catch(function (e) {
            if (e.indexOf("Unauthorized") >= 0)
                return obj.authenticateVOs(vos, token)
            else
                return Promise.reject(e)
        })

    }

    getTenants(token: string): any {
        var url = this.url + "/v2.0/tenants"
        var headers = {
            "Accept": "application/json",
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        }
        return new Promise(function (resolve, reject) {
            http.Get(url, headers).then(function (request) {
                var body = request.body
                body = JSON.parse(body.toString());
                if (body.error)
                    reject(body.error.message);
                else {
                    var tenants = []
                    for (var tenant in body.tenants) {
                        tenants.push(body.tenants[tenant].name)
                    }
                    resolve(tenants);
                }
            });
        });
   }
}
