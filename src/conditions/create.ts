/// <reference path="../../typings/tsd.d.ts" />
var Promise = require('bluebird');
var moment = require("moment");

export module CreateComputer {

  export function checkMachinesStateStartingLessThanMachinesToCreate(obj){
    return obj.db.listServers({site:obj.name,state:'CREATING'}).then((servers)=>{
      if(servers.length > obj.maxNumCreations)
        return Promise.reject(servers.length +" in state CREATING")
      else
        return Promise.resolve("checkMachinesStateStartingLessThanMachinesToCreate resolved")
    })
  }

  export function checkOneMachineAndIsRunning(obj){
    return obj.db.listServers({site:obj.name,state:{$nin:['DELETED','ERROR','ENDED']}})
    .then((servers)=>{
      if((servers.length) > 1 || (servers.length) === 0){
          return Promise.resolve()
      }else{
        if(servers[0].start !== undefined){
          return Promise.resolve("Machines executing")
        }else{
          return Promise.reject("Server running but not started to work: "+Math.round(moment().diff(moment.unix(servers[0].createdTime))/1000)+" seconds")
        }
      }
    })
  }

  export function checkRunningComputersHigherThanLimit(obj){
    return obj.db.listServers({site:obj.name, state:{$nin:['DELETED']}}).then((servers)=>{
      if((servers.length) >= obj.info.max_machines){
        return Promise.reject("Created "+ (servers.length) + " of "+ obj.info.max_machines);
      }else{
        return Promise.resolve("Created "+ (servers.length) + " of "+ obj.info.max_machines);
      }
    });
  }

  export function checkComputersCreatedThisRound(obj){
    if(obj.created >= obj.maxNumCreations)
      return Promise.reject("Created "+ obj.created +" of maximum "+ obj.maxNumCreations);
    else
      return Promise.resolve("Created "+ obj.created +" of maximum "+ obj.maxNumCreations);
  }
}
