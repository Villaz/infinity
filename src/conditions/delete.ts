/// <reference path="../../typings/tsd.d.ts" />
var Promise = require('bluebird');
var moment = require("moment");

export module DeleteComputer {


    export function deleteComputersInErrorEndedState(obj, ...loggers:any[])
    {
        return obj.db.listServers({ $and:[{site:obj.name}],$or: [{ state: 'ERROR' }, {state:'ENDED' }] })
          .each((server)=>{
            for(var i in loggers){
              var logger = loggers[i]
              logger.info(server.hostname +" in Error/Ended state. DELETING")
            }
          })
    }


    /*export function deleteComputersLostHeartbeat(obj, logger?) {
        var now_less_heartbeat = moment().subtract(obj.info.heartbeat,'seconds').unix()
        return obj.db.listServers({site:obj.name,
                                   state:'STARTED',
                                   heartbeat: {$lt:now_less_heartbeat}})
              .each((server)=>{
                if(logger !== undefined)
                  logger.info(server.hostname +" lost heartbeat ("+ (obj.info.heartbeat + (now_less_heartbeat - server.heartbeat))+")sg")
              })
    }*/


    export function deleteComputersNotStarted(obj, ...loggers:any[]) {
        var now = moment().subtract(obj.info.start_time,'seconds').unix()
        return obj.db.listServers({site:obj.name,
                                   state:{$in:['CREATING']},
                                   createdTime:{$lt:now}})
              .each((server)=>{
                for(var i in loggers){
                  var logger = loggers[i]
                  logger.info(server.hostname +" not started ("+ (moment().diff(moment.unix(server.createdTime))/1000)+")sg")
                }
              })
    }


    export function deleteComputerBootedButNotExecuting(obj, ...loggers:any[]){
      var now = moment().subtract(obj.info.start_time,'seconds').unix()
      return obj.db.listServers({site:obj.name,
                                 state:'BOOTED',
                                 start:{$exists:false},
                                 createdTime:{$lt:now}})
          .each((server)=>{
            for(var i in loggers){
              var logger = loggers[i]
              logger.info(server.hostname +" not started ("+ (moment().diff(moment.unix(server.createdTime))/1000)+")sg")
            }
          })
    }
}
