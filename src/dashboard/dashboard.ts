///<reference path='../../typings/node/node.d.ts' />
var Ganglia = require('../utils/ganglia').Ganglia
var Promise = require('bluebird')
var socketio = require('socket.io')

var obj = undefined

export class Dashboard{

  galaxies: any
  socketIOdict = {}
  io: any
  gangliaInfo = {}

  constructor(galaxies){
    this.galaxies = galaxies
    obj = this
    this.startSocketIO()
    this.getSites()
    this.start()
  }

  private start(){
    return obj.loadGangliaData()
      .then(obj.listInfoToDashboardGauges)
      .then(obj.getNodesResume).delay(5000).then(obj.start)
  }

  private loadGangliaData(){
      var galax = Object.keys(obj.galaxies)
      var ganglia = new Ganglia("http://egi-agm.cern.ch")
      var promises = []
      return Promise.resolve(galax).each(function(value){
        promises.push(ganglia.info({'r':'hour','cluster':value}).then((result)=>{
          obj.gangliaInfo[value] = result
          return Promise.resolve()
       }))
      }).then(()=>{
        promises.push(ganglia.info({'r':'hour'}).then((result)=>{
          obj.gangliaInfo['global'] = result
          return Promise.resolve()
        }))
      }).then(()=>{
        return Promise.all(promises)
      })
  }


  private startSocketIO(){
    this.io = socketio.listen(8081)
    var galax = Object.keys(obj.galaxies)
    for (var i in galax) {
      var nsp = this.io.of('/gauge'+galax[i])
      nsp.on('connection',function(socket){
        socket.emit('value',JSON.stringify({max:obj.galaxies[galax[i]]['max_machines'],actual:0}))
      })
      this.socketIOdict['gauge'+galax[i]] = nsp
    }

      var galaxies_aux = []
      for (var i in galax)
        galaxies_aux.push({site:galax[i],max:obj.galaxies[galax[i]]['max_machines']})

      var nsp = this.io.of('/gauges')
      nsp.on('connection',function(socket){
          socket.emit('value',galaxies_aux)
      })

      this.socketIOdict['sites'] = this.io.of('/sites')

      this.socketIOdict['nodes'] = {}
      var galax = Object.keys(obj.galaxies)
      for(var i in galax){
        this.socketIOdict['nodes'][galax[i]] = this.io.of('/nodes'+galax[i])
      }
      this.socketIOdict['nodes']['global'] = this.io.of('/nodes')
  }


  public listInfoToDashboardGauges(){
    var galax = Object.keys(obj.galaxies)
    return Promise.resolve(galax).each(function(value){
      if(obj.gangliaInfo[value] === undefined)
        return Promise.reject()
      var nodes = obj.gangliaInfo[value].load.nodes
      var nodeValue = nodes[nodes.length-1][0]
      obj.socketIOdict['gauge'+value].emit('value',Math.round(nodeValue))
    })
  }

  public getSites(){
    this.socketIOdict['sites'].on('connect',function(socket){
      socket.emit('value',Object.keys(obj.galaxies))
    })
  }

  public getNodesResume(){
    var galax = Object.keys(obj.galaxies)
    galax.push('global')
    return Promise.resolve(galax).each(function(value){
        var nodes = obj.gangliaInfo[value].load.nodes
        var min = obj.gangliaInfo[value].load['min']
        var ram = obj.gangliaInfo[value].memory.total
        obj.socketIOdict['nodes'][value].emit('value',{nodes:nodes,'1-min':min,'total':ram})
    })
  }
}
