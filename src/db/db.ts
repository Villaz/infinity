/// <reference path="../../typings/tsd.d.ts" />
var Promise = require("bluebird");
var MongoClient = require('mongodb').MongoClient;
var winston = require("winston");
var sprintf = require("sprintf-js").sprintf
var util = require("util");
import events = require("events");

/**
*@module DB
*/
export module DB{

    var obj: MongoDB;
    var EventEmitter = events.EventEmitter;

    class EventEmitter2 extends events.EventEmitter {
      constructor() { super(); }
    }
	/**
	*Class to connect and interact with MongoDB
	*@class MongoDB
	*/
	export class MongoDB extends events.EventEmitter{

		db:any;
		logger:any;
		host:string;
		username:string;
		password:string;
    publisher:any;
    channelPublish:any;

		/**
		*Constructor
		*@class MongoDB
		*@constructor
		*/
		constructor( host , username , password , logger ){
      super()
			this.logger = logger
			this.host = host
			this.username = username
      this.password = password
      obj = this
		}


		connect(site){
			return new Promise((resolve ,reject)=>{
            	var url = sprintf( "mongodb://%s:%s@%s", obj.username , obj.password , obj.host )
            	MongoClient.connect(url, function(err, db){
                  if(err !== null)
                    	reject(err)
                	else {
                      obj.db = db
                      resolve()
                  }
            	})
        	})
		}

		/**
    	*Inserts a new server in MongoDB
    	*@method insertServer
    	*@param {server} server Server to insert
    	*/
    	public insertServer(server){
            if(obj.logger !== undefined)
                obj.logger.info("Created server " + server.hostname + " "+ server.site)
            server.createdTime = Math.floor(Date.now() / 1000);
        	  return new Promise(function (resolve, reject) {
                var collection = obj.db.collection('servers');
                collection.insert(server, function(err, result) {
                  if (err)
                    reject(err)
                	else {
                    resolve(server)
                  }
            	});
        	});
    	}

        /**
        *Return a list of all servers represented by the query
        *@method listServers
        *@param {Object} query Query object
        *@return {Array}
        */
        public listServers(query: Object= {}) {
          query['id'] = {$exists: true}
            return new Promise(function (resolve, reject) {
                var collection = obj.db.collection('servers');
                collection.find(query).toArray((err, docs) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(docs);
                    }
                });
            });
        }

        public updateServer(server:string,fields:Object):any
        public updateServer(server:any, fields:Object):any {
            var hostname = undefined
            if(typeof server === "string")
              hostname = server
            else
              hostname = server.hostname

            if (obj.logger != undefined)
                obj.logger.info("Update server " + hostname)
        	  return new Promise( (resolve, reject) => {
            var collection = obj.db.collection('servers');
            collection.update({ 'hostname': hostname }, { $set: fields }, (err, result) => {
              if (err)
                reject(err)
              else
                resolve(result)
              });
            });
        }

        public sanatize(op){

          function sanatizeEnded(collection){
            return new Promise((resolve, reject)=>{
            collection.update({site:op.site,
                               end:{$exists:true},
                               state:{$nin:['ENDED','DELETED']}},
                              {$set:{state:'ENDED'}},(err, result)=>{
                                if(err) reject(err)
                                else resolve()
                              })
            })
          }

          function sanatizeStarted(collection){
            return new Promise((resolve, reject)=>{
            collection.update({site:op.site,
                               end:{$exists:false},
                               start:{$exists:true},
                               state:{$nin:['STARTED','DELETED']}},
                              {$set:{state:'STARTED'}},(err, result)=>{
                                if(err) reject(err)
                                else resolve()
                              })
            })
          }
          return new Promise((resolve,reject)=>{
            var collection = obj.db.collection('servers')
              Promise.all([sanatizeEnded(collection),sanatizeStarted(collection)]).then(()=>{
                resolve(op)
              })
          })
        }

        public mapReduce(map, reduce) {
            var obj: MongoDB = this;
            return new Promise((resolve, reject) => {
                var collection = obj.db.collection('servers');
                collection.mapReduce(map, reduce, { out: { inline: 1 } }, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            });
        }

    	/**
    	* Deletes a server from MongoDB
    	*@method deleteServer
    	*@param {server} server Server to delete
    	*/
      public deleteServer(server:string):any;
    	public deleteServer(server:any):any {
            var update = {hostname: ''};
            if(typeof server === "string"){
                update.hostname = server;
            }else{
                update.hostname = server.hostname
            }
            if(obj.logger != undefined)
        	    obj.logger.info("Deleted server " + update.hostname)

          return new Promise((resolve, reject) => {
            	var collection = obj.db.collection('servers');
            	collection.update(update,
                	{ $set: { state: 'DELETED' } }, function (err, result) {
                    if (err)
                        reject();
                    resolve();
            	});
        	});
        }

        public deleteAll() {
            return new Promise(function (resolve, reject) {
                var collection = obj.db.collection('servers');
                collection.remove({}, (err, result) => {
                    if (err)
                        reject(err);
                    else
                        resolve();
                });
            });
        }
	}
  util.inherits(DB, EventEmitter);
}
