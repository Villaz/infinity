///<reference path='../typings/node/node.d.ts' />
var sprintf = require("sprintf-js").sprintf
var nconf = require("nconf")
var winston = require("winston");
var Promise = require("bluebird");
var DB = require('./db/db').DB;
var m2 = require('./utils/m2');
var os = require("os");
var fs = require("fs");
var moment = require("moment");
var Papertrail = require('winston-papertrail').Papertrail;
var WinstonGraylog = require('./utils/winstongraylog.js').GrayLogger

nconf.file({ file: './infinity.conf' })


winston.cli()
var logger = new (winston.Logger)();
var grayLogger = new (winston.Logger)()

var obj: Infinity;

    /**
    * Class Infinity, provides the main logic
    *@class Infinity
    */
export class Infinity {

    /**
    * The interface to use to interact with the sites
    *@property connector
    *@type {Interface}
    */
    connector: any
    /**
    * The name of the site
    *@property name
    *@type {string}
    */
    name : string
    client:any
    info: any
    db: any
    servers: Array<any>
    created:number
    maxNumCreations:number
    totalRunningServers:number
    intervals : number

    /**
        * @class Infinity
        * @constructor constructor
        * @param {string} name Name of the galaxy(site).
        * @param {number} num_creations Number of VMs to create every iteration.
        */
    constructor(name, num_creations:number=5) {
        try{
          logger.add(winston.transports.DailyRotateFile, { level: 'debug', filename:"/var/log/"+ name + '.log' })
          logger.add(Papertrail, { level: 'info', host: 'logs2.papertrailapp.com', port: 12551, label:name });
          grayLogger.add(WinstonGraylog,{level:'debug', host:"infinitygraylog.cloudapp.net", port:12201, label:name})
        }catch(e){
          console.error(e)
        }
        this.name = name
        this.maxNumCreations = num_creations
        this.info = nconf.get("universe")["galaxies"][name]
        this.connector = require('./interfaces/' + this.info.type + '.js').Interface
        this.processEvents()
        this.created = 0
        this.db = new DB.MongoDB('ds047911.mongolab.com:47911/infinity' , 'luis' , 'Espronceda' , logger)
        this.intervals = 0
        obj = this
    }

    /**
    *Starts the object. Initiate a new iteration an repeat it every 600 seconds.
    *@method start
    */
    public start() {
        obj.db.connect(obj.name).then(( )=>{
            var info = obj.info
            info['name'] = obj.name
            obj.client = eval("new obj.connector." + obj.info.type.toUpperCase() + "(" + JSON.stringify(obj.info) + ");");
            var connected = obj.client.connect()
            connected.then(obj.start_interval)
        }).finally(function(err){
            if(err)
              throw err
        })
    }

    /**
    * Process the events sends by the master
    *@method processEvents
    */
    private processEvents()
    {
        var obj = this;
        process.on("message", function (msg)
        {
            msg = JSON.parse(msg);
            switch (msg.type)
            {
                case 'conf':
                    nconf.use('file', { file: '/path/to/some/config-file.json' });
                    obj.info = nconf.get("universe")["galaxies"][name]
                    break
                case 'end':
                  obj.client.deleteServer(msg.body.id)
                    .then(obj.loopCreateServers)
                    .catch((err)=>{logger.warn(err,{site:obj.name});})
                    .finally(()=>{
                    logger.info("End action of delete server",{site:obj.name})
                  });
            }
        });
    }

    /**
    *Starts a new interval.
    *First retrieves the list of all VMs, then delete the VMs with inconsistent state and finally create new ones.
    *@start_interval
    *
    */
    private start_interval()
    {
        logger.info("Starts loop",{site:obj.name})
        var op = {'site':obj.name,state:{$nin:['DELETED']}}
        if(obj.intervals%10 == 0)
          var list = obj.client.compute.list().then(obj.updateServers)
        else
          var list = obj.db.sanatize(op).then(obj.db.listServers)
        list.then(obj.print)
          .then(obj.deleteServers)
          .then(obj.loopCreateServers)
          .finally(function () {
            obj.intervals++
            logger.info("Ended loop",{site:obj.name});
            setTimeout(obj.start_interval, 60000);
          });
    }


    /**
    *Print a list of servers.
    *@method print
    *@param {list} servers List of servers
    */
    private print(servers) {
      logger.info("List servers",{site:obj.name});
      logger.info("HOST\t\tSTATE\tIPS\tBOOTED\tSTARTED")
      logger.info("---------------------------------")
      var state = {"STARTED":0, "STOPPED":0, "ERROR":0,'CREATING':0,'BOOTED':0}
      for (var i in servers) {
          if(servers[i].hostname.indexOf(obj.info.prefix) >= 0){
              var time_created = moment().diff(moment.unix(servers[i].createdTime)) / 1000
              var booted = 'boot' in servers[i]
              var started = 'start' in servers[i]
              logger.info(sprintf("%s\t%s\t%s\t%s\t%s\t%s(s)", servers[i].hostname ,
                servers[i].state ,
                servers[i].ips,
                booted,
                started,
                time_created))
              try{
                state[servers[i].state.toUpperCase()]++
              }catch(e){
                logger.error(e)
              }
          }
      }
      obj.totalRunningServers = state["STARTED"] + state['CREATING']
      logger.info("STARTED:" + state["STARTED"] + " BOOTED:"+ state["BOOTED"]+ " CREATING:" + state['CREATING']+" STOPPED:" + state['STOPPED'] + " ERROR:" + state['ERROR'],{site:obj.name});
      grayLogger.info("STARTED:" + state["STARTED"] + " BOOTED:"+ state["BOOTED"]+ " CREATING:" + state['CREATING']+" STOPPED:" + state['STOPPED'] + " ERROR:" + state['ERROR'])
      return Promise.resolve(servers)
    }

    private updateServers(servers){
      logger.info("Updating servers",{site:obj.name})
      return obj.db.listServers({site:obj.name,state:{$nin:['DELETED']}}).then((servers_db)=>{
        return obj.updateServerWithDifferentStatus(servers, servers_db).then(()=>{
          return obj.updateMachinesNotExists(servers, servers_db).then(()=>{
            return obj.db.listServers({site:obj.name,state:{$nin:['DELETED']}})
          })
        })
      })
    }


    private updateServerWithDifferentStatus(servers, servers_db){
      var promises = []
      for(var i in servers){
        var exists = false
        for(var j in servers_db){
          if(servers[i].hostname === servers_db[j].hostname){
            exists = true
            if(servers[i].state !== servers_db[j].state)
            {
              if(servers_db[j].state === 'ENDED'){
                grayLogger.warn(sprintf("%s deleted in DB deleting from site", servers_db[j].hostname))
                promises.push(obj.client.compute.delete(servers[i]))
                promises.push(obj.db.updateServer(servers_db[j].hostname,{state:'DELETED'}))
              }else{
                var state = servers[i].state
                if('boot' in servers_db[j])
                  state = 'BOOTED'
                if('start' in servers_db[j])
                  state = 'STARTED'
                if(servers[i].state === 'ERROR')
                  state = 'ERROR'
                logger.info(sprintf("Update %s to state %s", servers_db[j].hostname,state),{site:obj.name})
                grayLogger.info(sprintf("Update %s to state %s", servers_db[j].hostname,state))
                promises.push(obj.db.updateServer(servers_db[j].hostname,{state:state}))
                servers_db[j].hostname.state = state
              }
            }
            break
          }
        }
        if(!exists && servers[i].hostname.indexOf(obj.info.prefix) >= 0)
          promises.push(obj.client.compute.delete(servers[i]))
      }

      return Promise.all(promises)
    }


    private updateMachinesNotExists(servers, servers_db){
      var promises = []
      for(var i in servers_db){
        for(var j in servers){
          var exists = false
          if(servers_db[i].hostname === servers[j].hostname){
            exists = true
            break
          }
        }
        if(!exists)
          promises.push(obj.db.updateServer(servers_db[i].hostname,{state:'DELETED'}))
      }
      return Promise.all(promises)
    }

    /**
    *Delete the servers following differents conditions
    *@method deleteServers
    *@param {list} servers List of servers running in the site.
    */
    private deleteServers(servers) {
        logger.debug("Enter in delete servers",{site:obj.name});
        var del = require("./conditions/delete").DeleteComputer;
        var promises = [];
        var properties = Object.getOwnPropertyNames(del);
        for (var i in properties) {
            logger.debug("Executing "+ properties[i],{site:obj.name});
            promises.push(del[properties[i]](obj, logger, grayLogger)
              .each((server)=>
                {
                  logger.info(sprintf("Deleting server %s",server.hostname),{site:obj.name})
                  grayLogger.info(sprintf("Deleting server %s",server.hostname))
                  return obj.client.compute.delete(server)
                    .then(obj.db.deleteServer)
                    .catch((err)=>{
                      grayLogger.warn(err)
                      logger.warn(err,{site:obj.name})
                      return obj.db.deleteServer(server)
                    })
                })
            )
        }
        return Promise.all(promises);
    }



    private loopCreateServers(){
      return obj.checkConditionsToCreateServers()
        .then(obj.createServers)
        .then(obj.loopCreateServers)
        .catch((message)=>{ logger.error(message,{site:obj.name}); obj.created=0; return Promise.resolve(); })
    }


    private checkConditionsToCreateServers(){
      logger.debug("Enter conditions to create servers",{site:obj.name});
      var del = require("./conditions/create").CreateComputer;
      var promises = [];
      var properties = Object.getOwnPropertyNames(del);
      for (var i in properties) {
          logger.debug("Executing "+ properties[i],{site:obj.name});
          promises.push(del[properties[i]](obj));
      }
      return Promise.all(promises);
    }

    /**
    *Creates a new server.
    *If the number of VMs created by the server or the number of VMs is equals to the quota, the VM will not be created.
    * In other case the method reads the contextualization file and send the request to create the new VM.
    *@method createServers
    */
    private createServers( messages ){
        for(var i in messages)
          logger.info(messages[i],{site:obj.name})
        var fs = require("fs");
        var time = Math.floor(Date.now()/1000).toString()
        var name = obj.info.prefix+"-"+time;
        var info = JSON.parse(JSON.stringify(obj.info));
        var params = {  'id':name,
                        'starTime':time,
                        'server':nconf.get("universe")['server'],
                        'site': obj.name
                        }
        if(obj.info.cert != undefined)
            params['cert'] = fs.readFileSync(obj.info.cert)
        for(var param in obj.info.params){
          params[param] = obj.info.params[param]
        }

        return m2.resolveUserData(info.user_data, params)
                 .then(function(value){
                        info.user_data = value
                        logger.info("Creating server " + name,{site:obj.name})

                        return obj.client.compute.create(name,info).then((server)=>{
                          obj.created++
                          server.site = obj.name
                          grayLogger.info("Created "+name)
                          return obj.db.insertServer(server)
                        }).catch((err)=>{
                          grayLogger.warn(err)
                          return Promise.reject(err)
                        })
                    })
    }




    /**
    *Deletes all VMs
    *@method deleteAll
    */
    public deleteAll() {
        var self = this
        function deleteServers(servers) {
            var promises:any = []
            for (var i in servers) {
                self.client.compute.delete(servers[i]).then(function () {
                    promises.push(self.db.deleteServer(servers[i]));
                });
            }
            return Promise.all(promises);
        }

        function listServers() {
            return self.client.compute.list(false).then(deleteServers);
        }

        function afterConnectDB(db) {
            var dict = {name:self.name,url:self.info.url,proxy:self.info.proxy}
            self.client = eval("new obj.connector." + self.info.type.toUpperCase() + "(" + JSON.stringify(dict) + ");");
            var connected = self.client.connect()
            return connected.then(listServers);
        }

        return self.db.connect().then(afterConnectDB).catch(function (err) {
            logger.error("Error connecting DB "+ err);
            return Promise.reject(err);
        });
    }
}
