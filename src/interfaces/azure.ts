///<reference path='../../typings/tsd.d.ts' />
var http = require('../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf
var configuration = require('./azure/configurationSet.js')

  /**
  * Class Azure, provides an interface to use Azure
  *
  * @class Azure
  *
  */
  export class Azure{

    subscriptionId: string
    baseUrl:string = "https://management.core.windows.net/"
    cert:string
    key:string
    x_ms_version = '2014-10-01'

    location:AzureLocation
    service:Service
    vm:VM
    configurationSet:ConfigurationSet
    image: AzureImage
    storage:AzureStorage
    serviceCertificate:ServiceCertificate

    /**
    * Constructor
    * @method constructor
    * @param {Object} params params
    * @param {string} params.subscriptionId Subscription Id
    * @param {string} params.cert Certificate to connect to azure
    * @param {string} params.key Privatekey to authenticate in azure
    */
    constructor(subscriptionId:string, cert:string, key:string){
      this.subscriptionId = subscriptionId
      this.baseUrl += subscriptionId
      this.cert = cert
      this.key = key

      this.location = new (require('./azure/location.js')).AzureLocation(this)
      this.service = new (require('./azure/service.js')).Service(this)
      this.vm = new (require('./azure/vm.js')).VM(this)
      this.image = new (require('./azure/image.js')).AzureImage(this)
      this.storage = new (require('./azure/storage.js')).AzureStorage(this)
      this.serviceCertificate = new (require('./azure/serviceCertificate.js')).ServiceCertificate(this)
    }

}


var SSH = require('./azure/ssh.js')
/*var a = new Azure('baaf1202-15a5-4350-8c87-d13839de2d85',
 '/home/lvillazo/grid-certs/hostcert.cer','/home/lvillazo/grid-certs/hostkey.pem')
*/
var a = new Azure('cbe3cbba-9276-48d1-b08f-53aaeb72526a','C:\\Users\\Luis\\Downloads\\lvillazo.cer','C:\\Users\\Luis\\Downloads\\lvillazo.key')
var image_name = "2b171e93f07c4903bcad35bda10acf22__CoreOS-Stable-557.2.0"
var media_link = "https://infinityserver.blob.core.windows.net/images/infinityserver.vhd"
var osVirtualDisk = new (require('./azure/OSVirtualDisk.js')).OSVirtualDisk({mediaLink:media_link,sourceImageName:image_name})
var conf = new (require('./azure/configurationSet.js')).LinuxConfigurationSet({username:'lvillazo',password:'Espronceda1985$',disableSshPasswordAuthentication:false,hostname:'infinityserver'})

a.serviceCertificate.list('infinityserver').each((certificate)=>{
  conf.SSH.publicKeys.push(new SSH.Key({fingerprint:certificate.thumbprint ,path:"/home/lvillazo/.ssh/authorized_keys"}))
}).then(()=>{
  var role = new (require('./azure/vm.js')).Role(a)
    role.params = { name: 'infinityserver', oSVirtualHardDisk: osVirtualDisk}
    role.configurationSets.push(conf)
    var network = new configuration.NetworkConfigurationSet()
    network.inputEndpoint.push(new configuration.InputEndpoint({ localPort: 22, port: 22, name: 'SSH', protocol: 'TCP' }))
    role.configurationSets.push(network)
    a.vm.create("infinityserver", role).then((s)=> {console.log(s) }).catch((s) => {console.error(s) })
})

