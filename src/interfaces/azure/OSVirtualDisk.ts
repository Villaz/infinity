///<reference path='../../../typings/node/node.d.ts' />
///<reference path='../azure.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  export interface IOSVirtualDisk {
      hostCaching?:string
      diskLabel?:string
      diskName?:string
      mediaLink?:string
      sourceImageName?:string
      os?:string
      resizedSizeInGB?:Number
      remoteSourceImageLink?:string
  }

  export class OSVirtualDisk {
      private params:IOSVirtualDisk

      constructor(params: IOSVirtualDisk) {
          this.params = params
      }

      public toXML():string {
          var xml='<OSVirtualHardDisk>'
          if(this.params.hostCaching !== undefined)
              xml += vsprintf('<HostCaching>%s</HostCaching>',[this.params.hostCaching])
          if(this.params.diskLabel !== undefined)
              xml += vsprintf('<DiskLabel>%s</DiskLabel>',[this.params.diskLabel])
          if(this.params.diskName !== undefined)
              xml += vsprintf('<DiskName>%s</DiskName>',[this.params.diskName])
          if(this.params.mediaLink !== undefined)
              xml += vsprintf('<MediaLink>%s</MediaLink>',[this.params.mediaLink])
          if(this.params.sourceImageName !== undefined)
              xml += vsprintf('<SourceImageName>%s</SourceImageName>',[this.params.sourceImageName])
          if(this.params.os !== undefined)
              xml += vsprintf('<OS>%s</OS>',[this.params.os])
          if(this.params.remoteSourceImageLink !== undefined)
              xml += vsprintf('<RemoteSourceImageLink>%s</RemoteSourceImageLink>',[this.params.remoteSourceImageLink])
          if(this.params.resizedSizeInGB !== undefined)
              xml += vsprintf('<ResizedSizeInGB>%s</ResizedSizeInGB>',[this.params.resizedSizeInGB])
          xml += '</OSVirtualHardDisk>'
          return xml
      }
  }

