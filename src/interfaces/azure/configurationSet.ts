///<reference path='../../../typings/node/node.d.ts' />
///<reference path='../azure.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf
var SSH = require("./ssh.js")

  export class ConfigurationSet {
      public toXML(): string {return undefined}
   }

  export interface ILinuxConfigurationSet {
      hostname: string
      username: string
      password: string
      disableSshPasswordAuthentication?: boolean
      customData?: string
      computerName?: string
      automaticUpdates?: boolean
      timeZone?: string
  }

export interface IInputEndpoint {
    localPort:number
    port:number
    name:string
    protocol:string
}


  /**
  *Class LinuxConfigurationSet
  *@class LinuxConfigurationSet
  */
  export class LinuxConfigurationSet extends ConfigurationSet{
      private params:ILinuxConfigurationSet
      private ssh:SSH
      /**
      * Constructor
      * @method constructor
      * @param {ILinuxconfigurationSet} params
      */
      public constructor(params:ILinuxConfigurationSet){
          super()
          this.params = params
          this.ssh = new SSH.SSH()
      }

      public get SSH(){
        return this.ssh
      }

      /**
      *Returns and ConfigurationSets XML representation
      *@method toXML
      */
      public toXML() {
          var xml = '<ConfigurationSet i:type="LinuxProvisioningConfigurationSet">'
          xml += '<ConfigurationSetType>LinuxProvisioningConfiguration</ConfigurationSetType>'
          xml += vsprintf('<HostName>%s</HostName>', [this.params.hostname])
          xml += vsprintf('<UserName>%s</UserName>', [this.params.username])
          xml += vsprintf('<UserPassword>%s</UserPassword>', [this.params.password])
          if (this.ssh !== undefined)
              xml += this.ssh.toXML()
          if (this.params.disableSshPasswordAuthentication !== undefined)
              xml += vsprintf('<DisableSshPasswordAuthentication>%s</DisableSshPasswordAuthentication>', [this.params.disableSshPasswordAuthentication])
          if (this.params.customData !== undefined)
              xml += vsprintf('<CustomData>%s</CustomData>', [this.params.customData])
          xml += '</ConfigurationSet>'
          return xml
      }
  }


export class NetworkConfigurationSet extends ConfigurationSet {
    _inputEndpoints:Array<InputEndpoint>

    constructor() {
        super()
        this._inputEndpoints = []
    }

    get inputEndpoint() {
        return this._inputEndpoints
    }

    public toXML() {
        var xml = '<ConfigurationSet>'
        xml += '<ConfigurationSetType>NetworkConfiguration</ConfigurationSetType>'        
        xml += '<InputEndpoints>'
        for(var i in this._inputEndpoints)
            xml += this._inputEndpoints[i].toXML()
        xml += '</InputEndpoints>'
        xml += '</ConfigurationSet>'
        return xml
    }
}

export class InputEndpoint {
    params:IInputEndpoint

    constructor(params: IInputEndpoint) {
        this.params = params
    }

    public toXML() {
        var xml = '<InputEndpoint>'
        xml += vsprintf('<LocalPort>%s</LocalPort>',[this.params.localPort])
        xml += vsprintf('<Name>%s</Name>',[this.params.name])
        xml += vsprintf('<Port>%s</Port>',[this.params.port])
        xml += vsprintf('<Protocol>%s</Protocol>',[this.params.protocol])
        xml += '</InputEndpoint>'
        return xml
    }
}
