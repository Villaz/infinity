///<reference path='../../../typings/node/node.d.ts' />
///<reference path='../azure.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  /**
  * Class Image, provides method to manage images
  *
  * @class Image
  *
  */
  export class AzureImage {
      url: string
      azure: Azure

      /**
      * Constructor
      * @method constructor
      * @param {Azure} azure Azure object
      */
      constructor(azure: Azure) {
          this.azure = azure
          this.url = azure.baseUrl + "/services/images"
      }


      /**
      *List all images
      *@method list
      */
      public list() {
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          var self = this
          return new Promise((resolve, reject) => {
              http.Get(self.url, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
                  .then((response) => {
                      parseString(response.body.toString(), (err, response) => {
                          var images = []
                          for (var i in response.Images.OSImage) {
                              var b = response.Images.OSImage[i]
                              try {
                                  images.push({
                                      name: response.Images.OSImage[i].Name[0],
                                      category: response.Images.OSImage[i].Category[0],
                                      os: response.Images.OSImage[i].OS[0],
                                      family: response.Images.OSImage[i].ImageFamily[0],
                                  })
                              } catch (e) { }
                          }
                          resolve(images)
                      })
                  }).catch(reject)
          })
      }
  }

