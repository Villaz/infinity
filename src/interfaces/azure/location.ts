///<reference path='../../../typings/node/node.d.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf

 /**
  * Class Location, provides method to manage locations
  *
  * @class Location
  *
  */
  export class AzureLocation{

    azure:any

    /**
    * Constructor
    * @method constructor
    * @param {Azure} azure Azure object
    */
    constructor(azure:Azure){
      this.azure = azure
    }

    /**
    *Returns the list of all locations
    *@method list
    */
    public list(){
        var url = this.azure.baseUrl +"/locations"
      var headers = {'x-ms-version':this.azure.x_ms_version}
      return new Promise((resolve,reject)=>{
        http.Get(url,headers,  fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
          .then((response)=>{
            parseString(response.body.toString(), (err,result)=>{
              var locations = []
                for (var i in result.Locations.Location) {
                    locations.push({ name: result.Locations.Location[i].Name[0]})
                }

              resolve(locations)
            })
          }).catch(reject)
        })
      }

    /**
    * Create a new location
    *@method create
    *@param {string} name Name of the location
    *@param {string} label Label of the location
    *@param {string} location Geographic location where will be created
    *@param {string} [description] Description of the location
    */
    public create(name:string, label:string, location:string, description:string=undefined){
      var url = this.azure.baseUrl+"/affinitygroups"
      var xml:string = '<?xml version="1.0" encoding="utf-8"?>'
      xml += '<CreateAffinityGroup xmlns="http://schemas.microsoft.com/windowsazure">'
      xml += vsprintf("<Name>%s</Name>",[name])
      xml += vsprintf("<Label>%s</Label>",[new Buffer(label).toString('base64')])
      if(description !== undefined)
        xml += vsprintf("<Description>%s</Description>",[description])
      xml += vsprintf("<Location>%s</Location>",[location])
      xml +='</CreateAffinityGroup>'
      var headers = {'x-ms-version':this.azure.x_ms_version,'Content-Type':'application/xml'}
      return new Promise((resolve,reject)=>{
        http.Post(url,xml,headers, fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
        .then((response)=>{
          resolve(response)
        })
      })
    }

    /**
    * Deletes a location
    *@method delete
    *@param {string} name Name of the location
    */
    public delete(name:string){
      var url = this.azure.baseUrl+"/affinitygroups/"+name
      var headers = {'x-ms-version':this.azure.x_ms_version}
      return new Promise((resolve,reject)=>{
        http.Delete(url,headers, fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
        .then((response)=>{
          resolve(response)
        })
      })
    }

    /**
    * Returns the properties of a location
    *@method properties
    *@param {string} name Name of the location
    */
    public properties(name:string){
      var url = this.azure.baseUrl+"/affinitygroups/"+name
      var headers = {'x-ms-version':this.azure.x_ms_version}
      return new Promise((resolve,reject)=>{
        http.Get(url,headers, fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
        .then((response)=>{
          resolve(response)
        })
      })
    }

    /**
    *Updates a location
    *@method update
    *@param {string} name Name of the location
    *@param {string} [label] Label of the location
    *@param {string} [description] Description of the location
    */
    public update(name:string, label:string=undefined, description:string=undefined){
      var url = this.azure.baseUrl+"/affinitygroups/"+name
      var headers = {'x-ms-version':this.azure.x_ms_version}
      var xml = '<?xml version="1.0" encoding="utf-8"?>'
      xml += '<UpdateAffinityGroup xmlns="http://schemas.microsoft.com/windowsazure">'
      if(label !== undefined)
        xml += vsprintf("<Label>%s</Label>",[new Buffer(label).toString('base64')])
      if(description !== undefined)
        xml += vsprintf("<Description>%s</Description>",[description])
      xml += '</UpdateAffinityGroup>'
      return new Promise((resolve,reject)=>{
        http.Put(url, xml, headers, fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
        .then((response)=>{
          resolve(response)
        })
      })
    }
  }

