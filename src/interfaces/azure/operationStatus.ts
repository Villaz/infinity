///<reference path='../../../typings/node/node.d.ts' />
///<reference path='../azure.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  export class OperationStatus {
      url: string
      azure: any
      /**
      * Constructor
      * @method constructor
      * @param {Azure} azure Azure object
      */
      constructor(azure: any, request_id:string) {
          this.azure = azure
          this.url = azure.baseUrl + "/operations/"+ request_id
      }

      public getStatus() {
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          var self = this
          return new Promise((resolve, reject) => {
              http.Get(self.url, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
                  .then((response) => {
                      parseString(response.body.toString(), (err, response) => {
                          response = response.Operation
                          if (response.Status[0] === 'Failed')
                              reject(response.Error[0].Message[0])
                          else {
                              resolve(response.Status[0])
                          }
                      })
                  })
          })
      }

      public waitUntilSuccess() {
          var self = this
          var wait = () => {
              console.log("Waiting")
              return Promise.delay(10000).bind(self).then(self.getStatus)
           }

          return wait().then((status) => {
              console.log(status)
              if(status === 'Succeeded')
                  return Promise.resolve()
              else
                  return self.waitUntilSuccess()
           })
      }

  }

