///<reference path='../../../typings/node/node.d.ts' />

var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf

  export interface IService {
    name: string
    label: string
    description?:string
    location?:string
    affinityGroup?:string
    extendedProperties?:any
    reverse_dns_fqdn?:string
  }

  /**
  * Class Service, provides method to manage services
  *
  *@class Service
  *
  */
  export class Service{

    url:string
    azure:Azure

    /**
    * Constructor
    * @method constructor
    * @param {Azure} azure Azure object
    */
    constructor(azure:Azure){
      this.azure = azure
      this.url = azure.baseUrl +"/services/hostedservices"
    }

    /**
    *Lists all services
    */
    public list() {
      var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
      var self = this
      return new Promise((resolve, reject) => {
        http.Get(self.url, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
            .then((response) => {
                parseString(response.body.toString(), (err, response) => {
                    var services = []
                    for (var i in response.HostedServices.HostedService) {
                        services.push({ name: response.HostedServices.HostedService[i].ServiceName[0],
                            status: response.HostedServices.HostedService[i].HostedServiceProperties[0].Status[0],
                            location: response.HostedServices.HostedService[i].HostedServiceProperties[0].Location[0] })
                    }
                    resolve(services)
                })
            }).catch(reject)
        })
    }

    /**
    *Creates a new Service. At least location or affinityGroup must be expecified.
    *@method create
    *@param {string} name Name of the service
    *@param {string} label Label of the service
    *@param {string} [description] Description of the service
    *@param {string} [location] Location of the service
    *@param {string} [affinityGroup] Affinity group of the service
    *@param {string} [extendedProperties] Extended properties of the service
    *@param {string} [reverse_dns_fqdn] Specifies the DNS address to which the IP address of the cloud service resolves when queried using a reverse DNS query.
    */
    public create(params:IService){
      if (params.location === undefined && params.affinityGroup === undefined)
        throw new Error("Location or affinityGroup must be defined")
      if (params.location !== undefined && params.affinityGroup !== undefined)
        throw new Error("Only location or affinityGroup must be defined")

      var xml = '<?xml version="1.0" encoding="utf-8"?>'
      xml += '<CreateHostedService xmlns="http://schemas.microsoft.com/windowsazure">'
      xml += vsprintf('<ServiceName>%s</ServiceName>', [params.name])
      xml += vsprintf('<Label>%s</Label>', [new Buffer(params.label).toString('base64')])
      if (params.description !== undefined)
        xml += vsprintf('<Description>%s</Description>', [params.description])
      if (params.location !== undefined)
        xml += vsprintf('<Location>%s</Location>', [params.location])
      if (params.affinityGroup !== undefined)
        xml += vsprintf('<AffinityGroup>%s</AffinityGroup>', [params.affinityGroup])
      if (params.extendedProperties !== undefined)
        xml += params.extendedProperties
      if (params.reverse_dns_fqdn !== undefined)
        xml += vsprintf('<ReverseDnsFqdn>%s</ReverseDnsFqdn>', [params.reverse_dns_fqdn])
      xml += '</CreateHostedService>'

      var self = this
      var headers = {'x-ms-version':this.azure.x_ms_version, 'Content-Type':'application/xml'}
      return new Promise((resolve, reject)=>{
        http.Post(self.url,xml,headers,fs.readFileSync(self.azure.cert),fs.readFileSync(self.azure.key))
        .then((response)=>{
            if(response.status === 201)
                resolve()
            else
                reject()
        })
      })
    }

    /**
    * Deletes one server, if deleteDisks is true, the disks attached to the server will be deleted.
    *@param {string} name Service name.
    *@param {boolean} [deleteDisks] Set if disks should be deleted.
    */
    public delete(name:string,deleteDisks:boolean=false){
      var url=this.url+"/"+name
      var headers = {'x-ms-version':this.azure.x_ms_version}
      if(deleteDisks)
        url += "?comp=media"
      var self = this
      return new Promise((resolve, reject)=>{
        http.Delete(url,headers, fs.readFileSync(self.azure.cert),fs.readFileSync(self.azure.key))
        .then((response)=>{
          if(deleteDisks){
            //is asyncronous
          }else{
            resolve()
          }
        })
      })
    }

    /**
    *Checks Cloud Service Name Availability
    *@method checkNameAvailability
    *@param {string} name Name of the service
    */
    public checkNameAvailability(name:string){
      var url = this.url+"/operations/isavailable/"+name
      var headers = {'x-ms-version':this.azure.x_ms_version}
      var self = this
      return new Promise((resolve, reject)=>{
        http.Get(url,headers,fs.readFileSync(self.azure.cert),fs.readFileSync(self.azure.key))
        .then((response)=>{
          parseString(response.body.toString(),(err, response) =>{
            if ("Error" in response) {
                reject(response.Error.Message[0])
                return
            }
            response = response.AvailabilityResponse
            if(response.Result[0] === 'true')
              resolve(true)
            else
              reject(response.Reason)
          })
        })
      })
    }
  }

