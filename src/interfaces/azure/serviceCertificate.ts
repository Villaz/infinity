///<reference path='../../../typings/node/node.d.ts' />
///<reference path='../azure.ts' />
var http = require('../../utils/http')
var base = require('../azure')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  export interface IServiceCerticate{
    serviceName:string
    data:string
    format:string
    password?:string
  }

  export class ServiceCertificate{
    azure:Azure

    constructor(azure:Azure){
      this.azure = azure
    }


    public list(serviceName){
      var url = vsprintf("%s/services/hostedservices/%s/certificates",[this.azure.baseUrl,serviceName])
      var headers = {'x-ms-version':this.azure.x_ms_version}
      return new Promise((resolve,reject)=>{
        var certificates = []
        http.Get(url,headers,  fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
        .then((response)=>{
          parseString(response.body.toString(), (err,result)=>{
            if(result.Error !== undefined){
              reject(result.Error.Message)
              return
            }
              if (result.Certificates.Certificate === undefined) {
                  resolve(certificates)
                  return
              }
            Promise.resolve(result.Certificates.Certificate).each((certificate)=>{
              certificates.push({url:certificate.CertificateUrl,
                                 thumbprint:certificate.Thumbprint,
                                 data:certificate.Data})
            }).then(()=>{resolve(certificates)})
          })
        })
      })
    }


    public create(params:IServiceCerticate){
      var url =vsprintf("%s/services/hostedservices/%s/certificates",[this.azure.baseUrl,params.serviceName])
      var xml ='<?xml version="1.0" encoding="utf-8"?>'
      xml += '<CertificateFile xmlns="http://schemas.microsoft.com/windowsazure">'
      xml += vsprintf('<Data>%s</Data>',[new Buffer(params.data).toString('base64')])
      xml += vsprintf('<CertificateFormat>%s</CertificateFormat>',[params.format])
      if(params.format == 'pfx')
        xml += vsprintf('<Password>%s</Password>',[params.password])
      xml +='</CertificateFile>'
      var headers = {'x-ms-version':this.azure.x_ms_version,'Content-Type': 'application/xml'}
      return new Promise((resolve,reject)=>{
        http.Post(url, xml, headers, fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
        .then((result)=>{
            resolve()
        }).catch((error)=>{
          reject(error.body.toString())
        })
      })
    }
  }
