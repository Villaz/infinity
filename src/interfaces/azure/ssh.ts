///<reference path='../../../typings/node/node.d.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  export interface IKey{
    fingerprint:string
    path:string
  }

  export class Key{

    params:IKey

    constructor(params:IKey){
      this.params = params
    }


    /**
    *Returns the Key in XML format
    */
    public toXML():string{
      var xml = vsprintf('<Fingerprint>%s</Fingerprint>',[this.params.fingerprint])
      xml += vsprintf('<Path>%s</Path>',[this.params.path])
      return xml
    }

    public static generateCertificateFingerprint(path){
      var exec = require('child_process').exec
      return new Promise((resolve,reject)=>{
        var command = vsprintf('openssl x509 -in %s -noout -fingerprint',[path])
        var child = exec(command,function (error, stdout, stderr) {
          if(error)
            reject(error)
          else{
            resolve(stdout.substr(stdout.indexOf('=')+1).replace(/:/g,''))
          }
        })
      })
    }
  }

  export class SSH{
    private _publicKeys: Array<Key>
    private _keyPairs:Array<Key>

    constructor(){
      this._publicKeys = []
      this._keyPairs = []
    }

    get publicKeys(){
      return this._publicKeys
    }

    get keyPairs(){
      return this._keyPairs
    }

    public toXML():string{
      var xml = '<SSH>'
      if(this._publicKeys.length > 0){
        xml += '<PublicKeys>'
        for(var i in this._publicKeys){
          xml += '<PublicKey>'
          xml += this._publicKeys[i].toXML()
          xml += '</PublicKey>'
        }
        xml += '</PublicKeys>'
      }
      if(this._keyPairs.length > 0){
        xml += '<KeyPairs>'
        for(var i in this._keyPairs){
          xml += '<KeyPair>'
          xml += this._keyPairs[i].toXML()
          xml += '</KeyPair>'
        }
        xml += '</KeyPairs>'
      }
      xml += '</SSH>'
      return xml
    }
  }
