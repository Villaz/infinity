///<reference path='../../../typings/node/node.d.ts' />
///<reference path='../azure.ts' />
var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  export interface  IStorage {
      serviceName: string
      description?: string
      affinityGroup?:string
      location?:string
      geoReplication?: boolean
      secondaryReadEnabled?: boolean
      accountType: string
  }


  /**
  *Class Storage
  *@class Storage
  */
  export class AzureStorage {
      url: string
      azure: Azure

      /**
      * Constructor
      * @method constructor
      * @param {Azure} azure Azure object
      */
      constructor(azure: Azure) {
          this.azure = azure
          this.url = azure.baseUrl + "/services/storageservices"
      }


      /**
      *Lists all storages in the susbcription
      *@method list
      */
      public list() {
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          var self = this
          return new Promise((resolve, reject) => {
              http.Get(self.url, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
                  .then((response) => {
                      parseString(response.body.toString(), (err, response) => {
                          var storages = []
                          for (var i in response.StorageServices.StorageService) {
                              try {
                              storages.push({
                                  name: response.StorageServices.StorageService[i].ServiceName[0]
                                 })
                              } catch (e) { }
                          }
                          resolve(storages)
                      })
                  }).catch(reject)
          })
      }

      /**
      *Creates a new storage
      *@method create
      */
      public create(params:IStorage) {
          if ((params.affinityGroup === undefined && params.location === undefined) || (params.affinityGroup !== undefined && params.location !== undefined))
              return Promise.reject("AffinityGroup or location must be defined")

          var xml = '<?xml version="1.0" encoding="utf-8"?>'
          xml += '<CreateStorageServiceInput xmlns = "http://schemas.microsoft.com/windowsazure" >'
          xml += vsprintf('<ServiceName>%s</ServiceName>', [params.serviceName])
          xml += vsprintf('<Label>%s</Label>', [new Buffer(params.serviceName).toString("base64")])
          if (params.description !== undefined)
              xml += vsprintf('<Description>%s</Description>', [params.description])
          if (params.affinityGroup !== undefined)
              xml += vsprintf('<AffinityGroup>%s</AffinityGroup>', [params.affinityGroup])
          if (params.location !== undefined)
              xml += vsprintf('<Location>%s</Location>', [params.location])
          if (params.geoReplication !== undefined)
              xml += vsprintf('<GeoReplicationEnabled>%s</GeoReplicationEnabled>', [params.geoReplication])
          if (params.secondaryReadEnabled !== undefined)
              xml += vsprintf('<SecondaryReadEnabled>%s</SecondaryReadEnabled>', [params.secondaryReadEnabled])
          if (params.accountType !== undefined)
              xml += vsprintf('<AccountType>%s</AccountType>', [params.accountType])
          xml += vsprintf('</CreateStorageServiceInput>')
          console.log(xml)
          var self = this
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          return new Promise((resolve, reject) => {
              http.Post(self.url, xml, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
                  .then((response) => {
                      var request_id = response.headers['x-ms-request-id']
                      new (require('operationStatus.js')).OperationStatus(self.azure, request_id).waitUntilSuccess().then(resolve).catch(reject)
                  }).catch((response) => {
                      parseString(response.body.toString(), (err, response) => {
                          reject(response.Error.Message[0])
                      })
                  })
          })
      }
  }
