///<reference path='../../../typings/tsd.d.ts' />

var http = require('../../utils/http')
var Promise = require('bluebird')
var fs = require('fs')
var parseString = require('xml2js').parseString;
var vsprintf = require("sprintf-js").vsprintf


  export interface IRole {
      name:string
      mediaLocation?:string
      resourceExtensionReferences?:any
      vmImageName?:string
      availabilitySetName?:string
      dataVirtualHardDisks?:any
      oSVirtualHardDisk?:OSVirtualDisk
      roleSize?:any
      provisionGuestAgent?:boolean
      vmImageInput?:any
  }


  /**
  *Class Role
  *@class Role
  */
  export class Role {
      url: string
      azure: Azure
      private _params:IRole
      private _configurationSets: Array<ConfigurationSet>

      /**
      * Constructor
      * @method constructor
      * @param {Azure} azure Azure object
      */
      constructor(azure: Azure) {
          this.azure = azure
          this.url = azure.baseUrl
          this._configurationSets = new Array<ConfigurationSet>()
      }

      /**
      *Set params value
      *@method params
      *@param {IRole} params
      */
      set params(params: IRole) {
          this._params = params
      }

      /**
      *Get the name of the role
      *@return {string} Name
      */
      get name() {
          return this._params.name
      }

      /**
      *Gets the configurationSets
      *@method configurationSets
      */
      get configurationSets() {
        return this._configurationSets
      }

      /**
      *Creates a new Role
      *@method create
      *@param {string} serviceName
      *@param {IRole} params Role properties
      */
      public create(serviceName:string,params:IRole=undefined) {
          if(params !== undefined)
              this._params = params
          var url = vsprintf("%s/services/hostedservices/%s/deployments/%s/roles",[this.url, serviceName, this._params.name])
          var xml = '<PersistentVMRole xmlns="http://schemas.microsoft.com/windowsazure" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'
          xml += this.toXML()
          xml += '</PersistentVMRole>'
          
          var self = this
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          return new Promise((resolve, reject) => {
              http.Post(url, xml, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
                  .then((response) => {
                      if (response.status === 201)
                          resolve()
                      else
                          reject(response.body.toString())
                  })
          })

      }

      /**
      * List all Roles/VMs in one service
      *@method list
      *@param {string} serviceName
      */
      public list(serviceName: string){
          var roles = []
          var url = vsprintf("%s/services/hostedservices/%s?embed-detail=true",[this.url, serviceName])
          var self = this
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          return new Promise((resolve, reject) => {
              http.Get(url, headers, fs.readFileSync(self.azure.cert), fs.readFileSync(self.azure.key))
                  .then((response) => {
                      parseString(response.body.toString(), (err, response) => {
                          Promise.resolve(response.HostedService.Deployments[0].Deployment[0].RoleInstanceList[0].RoleInstance).each((role) => {
                              var vip = "0.0.0.0"
                              try {
                                  vip = role.InstanceEndpoints[0].InstanceEndpoint[0].Vip[0]
                              } catch (e) { }
                              roles.push({name:role.HostName[0],
                                          state: role.PowerState[0],
                                          ip:role.IpAddress[0],
                                          vip:vip})
                          }).then(() => { resolve(roles) })
                      })
                  })
          })
      }

      /**
      *Returns an XML representation
      *@method toXML
      */
      public toXML():string {
          var xml = vsprintf('<RoleName>%s</RoleName>',[this._params.name])
          xml += '<RoleType>PersistentVMRole</RoleType>'
          xml += '<ConfigurationSets>'
          for (var i in this._configurationSets) {
              xml += this._configurationSets[i].toXML()
          }
          xml += '</ConfigurationSets>'
          if(this._params.vmImageName !== undefined)
              xml += vsprintf('<VMImageName>%s</VMImageName>',[this._params.vmImageName])
          if(this._params.mediaLocation !== undefined)
              xml += vsprintf('<MediaLocation>%s</MediaLocation>',[this._params.mediaLocation])
          if(this._params.availabilitySetName !== undefined)
              xml += vsprintf('<AvailabilitySetName>%s</AvailabilitySetName>',[this._params.availabilitySetName])
          if(this._params.roleSize !== undefined)
              xml +=vsprintf('<RoleSize>%s</RoleSize>',[this._params.roleSize])
          if(this._params.provisionGuestAgent !== undefined)
              xml +=vsprintf('<ProvisionGuestAgent>%s</ProvisionGuestAgent>',[this._params.provisionGuestAgent])
          if(this._params.oSVirtualHardDisk !== undefined)
              xml += this._params.oSVirtualHardDisk.toXML()
          return xml
      }
  }


  /**
  *Class VM
  *@class VM
  */
  export class VM extends Role{

      /**
      * Constructor
      * @method constructor
      * @param {Azure} azure Azure object
      */
      constructor(azure: Azure) {
          super(azure)
          this.azure = azure
          this.url = azure.baseUrl
      }


      public create(serviceName, role:Role) {
          var xml = '<Deployment xmlns="http://schemas.microsoft.com/windowsazure" xmlns:i = "http://www.w3.org/2001/XMLSchema-instance" >'
          xml += vsprintf("<Name>%s</Name>",[role.name])
          xml += "<DeploymentSlot>Production</DeploymentSlot>"
          xml += vsprintf("<Label>%s</Label>",[role.name])
          xml += '<RoleList>'
          xml += '<Role>'
          xml += role.toXML()
          xml += '</Role>'
          xml += '</RoleList>'
          xml += '</Deployment>'
          var fs = require("fs")
          fs.writeFileSync("create.xml",xml)
          var url = vsprintf("%s/services/hostedservices/%s/deployments",[this.url, serviceName])

          var self = this
          var headers = { 'x-ms-version': this.azure.x_ms_version, 'Content-Type': 'application/xml' }
          return new Promise((resolve, reject) => {
              http.Post(url, xml, headers, fs.readFileSync(this.azure.cert), fs.readFileSync(this.azure.key))
                  .then((response) => {
                      var request_id = response.headers['x-ms-request-id']
                      new (require('./operationStatus.js')).OperationStatus(self.azure, request_id).waitUntilSuccess().then(resolve).catch(reject)
                  }).catch((response) => {
                      parseString(response.body.toString(), (err, response) => {
                          reject(response.Error.Message[0])
                      })
                  })
          })

      }
  }
