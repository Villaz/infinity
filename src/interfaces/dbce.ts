///<reference path='../../typings/node/node.d.ts' />
var Promise = require("bluebird");
var http = require("../utils/http");
var models = require("../models/models");
var fs = require("fs");
var uuid = require('node-uuid');
var winston = require('winston');

var logger = new (winston.Logger)();

export module Interface {
    /**
    * Class DBCE.
    * Provides an interface to use DBCE
    * @class DBCE
    *
    */
    export class DBCE {

        endpoint: string
        headers
        provider = {}
        name_provider: string
        compute = undefined
        template = undefined

         /**
        * @class DBCE
        * @constructor constructor
        * @param {Object} params params
        * @param {Object} params.name  name Name of the site.
        * @param {Object} params.url  url Url of the site.
        * @param {Object} params.username username Username to connect to DBCE.
        * @param {Object} params.password password Password to connect to DBCE.
        */
        constructor(params: {name:string; url: string; username: string; password:string}) {
            logger.debug("Created DBCE client to:" + params.name)
		        this.endpoint = params['url']
            this.headers = {
                'Authorization': 'Basic ' + new Buffer(params['username'] + ':' + params['password']).toString('base64'),
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
		        this.name_provider = params.name;
        }

        /**
        *Connects to DBCE and retrieve information about the diferent providers.
        *@method connect
        */
        public connect() {
          logger.info("Connecting DBCE");
          var obj = this;
		      return new Promise(function (resolve, reject) {
              obj.loadProviders().then(function (provider) {
                  obj.provider = provider;
				          obj.compute = new ComputerActions(obj.endpoint, obj.headers, provider);
				          obj.template = new TemplateActions(obj.endpoint, obj.headers, provider);
				          resolve();
			        }).catch(function (err) {
                  reject(err);
              });
          });
        }

        /**
        *Load information about all providers
        *@method loadProviders
        */
        private loadProviders() {
            var url = this.endpoint + '/ui/multicloud/providerLocations/'
		        var obj = this
		        var promises = []
		        logger.debug("Loading providers:" + url)
		        return new Promise(function (resolve, reject) {
                http.Get(url, obj.headers).then(function (response) {
                    var providersLocation = JSON.parse(response.body)['providerLocations']
        				    providersLocation.forEach(function (provider) {
                        promises.push(obj.loadProvider(provider))
				            });
                    Promise.any(promises).then(function (provider) {
                        logger.debug("Loaded provider.", { provider: provider.name })
		          			    resolve(provider)
				            }).catch(function () {
                        reject("The provider " + obj.name_provider + " not exists")
				            });
                });
            });
        }

        /**
        * Loads information about one provider
        *@method loadProvider
        *@param {provider} provider provider to load the information.
        */
        private loadProvider(provider) {
            var obj = this
		        var href = (provider['provider']['href']).replace('cxf', 'ui')
		        return new Promise(function (resolve, reject) {
                http.Get(href, obj.headers).then(function (response) {
                    logger.debug("Loading providers:" + JSON.parse(response.body).name)
				            if (obj.name_provider == JSON.parse(response.body).name) {
                        obj.loadPools(provider).then(function (pool) {
                            resolve({
                                     'name': JSON.parse(response.body).name,
                                     'id': provider['uid'],
                                     'pool': pool
                                    });
                        });
                    }else
                        reject()
			          });
            });
        }


        /**
        *Loads the different provider pools
        *@method loadPools
        *@param {provider} provider Provider to load the pools
        */
        private loadPools(provider) {
            var obj = this
		        var urlPools = this.endpoint + '/ui/capacity/capacityPools'
		        logger.debug("Loading pools:" + urlPools)
		        return new Promise(function (resolve, reject) {
                http.Get(urlPools, obj.headers).then(function (response) {
                    var promises = []
				            var capacityPools = JSON.parse(response.body)['capacityPools']
				            Promise.resolve(capacityPools).map(function (pool) {
					              return {
                               'id': pool['providerLocation']['href'],
                               'href': pool['capacities']['href'].substr(0, pool['capacities']['href'].lastIndexOf('/'))
                        }
				            }).filter(function (pool) {
                        if (pool.id == provider.id)
						                return true
					              return false
				            }).then(function (pool) {
                        logger.debug("Loaded pool", { pool: pool[0].id })
					              resolve(pool[0])
				            });
                });
            });
        }
    }


    export interface Action {
        describe(id: string): any;
        list();
        delete(id: string): any;
    }

    /**
    *Class computerActions, contains all the methods to manage VMs in DBCE
    *@class ComputerActions
    */
    class ComputerActions implements Action {

      endpoint: string
	    url: string
	    headers
	    provider

        /**
        *Constructor
        *@class ComputerActions
        *@constructor
        *@param {string} endpoint Endpoint to connect
        *@param {list} headers Http headers to use.
        *@param {string} provider Name provider
        */
	    constructor(endpoint: string, headers, provider) {
        this.endpoint = endpoint
		    this.url = endpoint + '/ui/iaas/machines'
		    this.headers = headers
		    this.provider = provider
	    }

        /**
        * Return a description of the VM
        *@method describe
        *@param {string} id . VM identifier
        *@return {Computer}
        */
        public describe(id: string): any { }


        /**
        * Return a list of all VMs.
        *@method list
        */
        public list() {
            var obj = this
		        var url = this.url + "?provider-location-uid=" + this.provider.id
            logger.debug("List servers for " + this.provider.id)
            return new Promise(function (resolve, reject) {
              http.Get(url, obj.headers).then(function (response) {
                try {
                  response = JSON.parse(response.body);
                } catch (err) {
                  };
                  var machines = []
				          for (var i in response.machines) {
                    var machine = new models.Compute(response.machines[i].id.substr(response.machines[i].id.lastIndexOf("/") + 1))
					          machine.hostname = response.machines[i].name
					          machine.state = response.machines[i].state
					          machine.memory = response.machines[i].memory
					          machine.disk = response.machines[i].disks
                    machine.ips = []
                    try {
                      var ips: Array<any> = response.machines[i].networkInterfaces.machineNetworkInterfaces[0].addresses.machineNetworkInterfaceAddresses
                    } catch (e) {
                      if(machine.state !== 'ERROR')
                        logger.warn(machine.hostname + " doesn't have ip, maybe created right now");
                      var ips = []
                    }
                    for (var j in ips)
                      machine.ips.push(ips[j].address.ip)
					            machines.push(machine)
				         }
                 resolve(machines)
			         }).catch(function (response) {
                  if (typeof response === "string") {
                    reject(response);
                  }
                  else if (response.error == 502)
                   reject("Internal Error, problem with proxy, contact provider");
                  else
                   reject(response.body.toString());
               });
            });
        }

        /**
        *Create a new VM
        *@method create
        *@param {string} title VM name
        *@param {Object} params
        *@param {Object} params.image Image to create the VM
        *@param {Object} params.flavor Flavor to create the VM
        *@param {Object} params.credentials Array with the credentials to use to connect to the VM
        *@param {Object} params.user_data User data to use to contextualize the VM.
        *@return {Computer}
        */
        create(title: string, params: { image: string; flavor: string; credentials: Array<any>;user_data:string}){
            var templateAction = new TemplateActions(this.endpoint, this.headers, this.provider)
            var obj = this
            var fs = require("fs")
            return new Promise(function (resolve, reject) {
                templateAction.find(params.image, params.flavor).then(function (image) {
                    if(image == undefined)
                    {
                        reject("Incorrect image");
                        return
                    }
                    if(params.user_data)
                        image.user_data = params.user_data

                    var json_request = {
                        'allocatedFrom': {
                            'resourceURI': 'http://schemas.zimory.com/cimi/zcap/1/CapacityPool',
                            'href': obj.provider.pool.href
                        },
                        "allocationSpecs": [
                            {
                                "resourceURI": "http://schemas.zimory.com/cimi/zcap/1/AllocationSpec",
                                "aspect": "http://schemas.dmtf.org/cimi/1/aspect/disk",
                                "classOfService": "bronze",
                                "classOfPerformance": "standard"
                            },
                            {
                                "resourceURI": "http://schemas.zimory.com/cimi/zcap/1/AllocationSpec",
                                "aspect": "http://schemas.dmtf.org/cimi/1/aspect/memory",
                                "classOfService": "bronze",
                                "classOfPerformance": "standard"
                            },
                            {
                                "resourceURI": "http://schemas.zimory.com/cimi/zcap/1/AllocationSpec",
                                "aspect": "http://schemas.zimory.com/dbce/1/aspect/compute",
                                "classOfService": "bronze",
                                "classOfPerformance": "standard"
                            }],
                        'description': title,
                        'machineTemplate': image.json(),
                        'name': title,
                        'resourceURI': "http://schemas.dmtf.org/cimi/1/MachineCreate"
                    };
                    if('credentials' in params){
                        if (params.credentials.length > 0)
                            json_request['credentials'] = {
                                'resourceURI': 'http://schemas.dmtf.org/cimi/1/CredentialCollection',
                                'credentials': []
                            }

                        for (var i in params.credentials)
                            json_request['credentials']['credentials'].push({
                                'resourceURI': 'http://schemas.dmtf.org/cimi/1/Credential',
                                'keyData': params.credentials[i]
                            });
                    }
                    var fs = require("fs");
                    fs.writeFileSync("fichero.json", JSON.stringify(json_request));
                    http.Post(obj.url, JSON.stringify(json_request), obj.headers).then(function (response) {
                        response = JSON.parse(response.body)
                        if ("resourceURI" in response && response["resourceURI"].indexOf("Error") >= 0) {
                            reject(response.message);
                        }
                        var compute = new models.Compute(response.id)
                        compute.hostname = response.name
					              compute.state = response.state
					              compute.memory = response.memory
					              compute.disk = response.disks
					              compute.ips = response.networkInterfaces
					              resolve(compute)
				            }).catch(function (err) {
                        if (typeof err === "string") {
                            reject(err);
                        }
                        if (err.body)
                            reject("Exceeds hard limit of capacity");
                        else
                            reject(err.toString());
                    });
                });
            });
        }

        /**
        *Delete a VM
        *@method delete
        *@param {Object} id. VM identifier
        */
        delete(server:any):any{
          var id = server.id.substr(server.id.lastIndexOf("/")+1);
          var url = this.url +"/"+ id + "?provider-location-uid=" + this.provider.id;
          var obj = this;
          logger.debug("Trying to delete server " + server.hostname)
          return new Promise(function (resolve, reject) {
            http.Delete(url, obj.headers).then(function (response) {
              logger.debug("Deleted server " + server.hostname)
              resolve(server);
            }).catch((err)=>{
              if(err == 404){
                logger.warn("Server "+ server.hostname +" was erased before");
                resolve(server);
              }else{
                reject(err);
              }
            });
          });
        }


    }

    /**
    *Class TemplateActions, contains all the methods to manage templates in DBCE
    *@class TemplateActions
    */
    class TemplateActions implements Action {

        url: string
	    headers
	    provider

        /**
        *Constructor
        *@class TemplateActions
        *@constructor
        *@param {string} endpoint Endpoint to connect
        *@param {list} headers Http headers to use.
        *@param {string} provider Name provider
        */
	    constructor(endpoint: string, headers, provider) {
          this.url = endpoint + '/ui/iaas/machineTemplates'
		      this.headers = headers
		      this.provider = provider
	    }


        /**
        * Return a description of the template
        *@method describe
        *@param {string} id . template identifier
        *@return {Template}
        */
        describe(id: string): any { }


        /**
        * Return a list of all templates.
        *@method list
        */
        list() {
            var obj = this;
		        var url = this.url + "?provider-location-uid=" + this.provider.id;
		        logger.debug("List templates for " + this.provider.id);
		        return http.Get(url, obj.headers).then(function (response) {
                   response = JSON.parse(response.body);
                   var templates = []
			    	       for (var i in response.machineTemplates) {
                      templates.push(new models.MachineTemplate(response.machineTemplates[i]))
				           }
                   return Promise.resolve(templates)
			      });
        }


        /**
        *Find a template given a name and a flavor.
        *@method find
        *@param {string} name Name of the template.
        *@param {string} flavor Flavor of the template.
        *@return {Template}
        */
        find(name:string, flavor:string) {
            var obj = this;
		        logger.debug("Filtering templates by name: " + name + " ,flavor:" + flavor);
		        return new Promise(function (resolve, reject) {
                obj.list().filter(function (template) {
                    logger.debug("machine_image.name: " + template.machine_image.name + " ; machine_config.name: " + template.machine_config.name);
				            if (template.machine_image.name == name && template.machine_config.name == flavor)
                        return true;
                    else
                        return false;
                }).then(function (templates) {
                    resolve(templates[0]);
                });
            });
        }


        /**
        *Delete a template
        *@method delete
        *@param {string} id. template identifier
        */
        delete(id: string): any {
            var url = this.url + "?provider-location-uid=" + this.provider.id;
    	      var obj = this;
            return http.Delete(url, obj.headers).then(function (response) {
                return Promise.resolve(JSON.parse(response));
            });
        }
    }
}
