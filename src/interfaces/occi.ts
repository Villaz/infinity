var Promise = require("bluebird");
var openStack = require("../auth/openstack");
var http = require("../utils/http");
var models = require("../models/models");
var fs = require("fs");
var uuid = require('node-uuid');
var winston = require("winston");

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({ level: 'debug' }),
    ]
});

var obj:Interface.OCCI = undefined

/**
*@module Interface
**/
export module Interface {

    var State = {CREATING:"CREATING",
                 BOOTED:"BOOTED",
                 STARTED:"STARTED",
                 ENDED:"ENDED",
                 DELETED:"DELETED",
                 ERROR:"ERROR"}

    /**
    * Class Occi, provides an interface to use OCCI
    *
    * @class OCCI
    *
    */
    export class OCCI {

        url: string
        cert: string
        token: string
        compute: Action

        /**
        * Constructor
        * @method constructor
        * @param {dictionary} {name,url,cert} Name of the site, url of the site, and cert to use with SSL
        */
        constructor(params: { name: string; url: string; proxy: string }) {
            logger.debug("Created Occi client to:" + params.name)
            this.url = params.url
            this.cert = params.proxy
            obj = this
        }

        /**
        * Connects to the server, if the server requires authorization, the authorization is done.
        * @method connect
        * @return {Promise} A resolve promise when the connection is done.
        */
        connect(): any {
            var obj = this;
            return obj.getKeystoneUrl().then((url)=>{
                //If url is not undefined we need to authenticate via Openstack
                if(url != undefined)
                {
                    return new openStack.Openstack(url, obj.cert).auth()
                      .then((token)=>{
                            obj.token = token
                            obj.compute = new ComputerActions(obj.url, token, obj.cert)
                            return Promise.resolve()
                      }).catch((err)=>{
                            logger.warn(err)
                            return Promise.reject(err)
                        });
                    }else{
                        obj.compute = new ComputerActions(obj.url, undefined, obj.cert, obj);
                        return Promise.resolve();
                    }
                });
            }

        /**
        * Called when the authentication is via Openstack
        *@method getKeystoneUrl
        *@return {Promise} The final url to communicate via Occi.
        */
        private getKeystoneUrl() {
            var url = this.url
            return new Promise(function (resolve, reject) {
                http.Head(url).then(function (headers) {
                    if('www-authenticate' in headers)
                    {
                        var authenticate = headers['www-authenticate']
                        authenticate = authenticate.substr(authenticate.indexOf("=") + 1).replace(/\s/g, '')
                        authenticate = authenticate.substr(0, authenticate.length - 1)
                        authenticate = authenticate.substr(1)
                        authenticate = authenticate
                        resolve(authenticate)
                    }else{
                        resolve(undefined)
                    }
                });
            });
        }
    }

    /**
    *@interface Action
    */
    export interface Action {
        describe(id: string): any;
        list();
        delete(id: string): any;
    }

    /**
    * Class to execute computer actions
    *@class ComputerActions
    */
    class ComputerActions implements Action {
        url: string
        token: string
        cert: string
        client: any

        /**
        *@class ComputerActions
        *@constructor
        *@param {string} url Url to connect
        *@param {string} [token] token to use to connect to authenticated servers
        *@param {string} [cert] Path to the certificate to use when connect via SSL
        */
        constructor(url: string, token:string=undefined, cert: string= undefined, client:any=undefined) {
            this.url = url
            this.token = token
            this.cert = cert
            this.client = client
        }

        /**
        * List all computers
        *@method list
        */
        list(extended=true) {
            var url = this.url + "/compute/"
            var headers = {
                        "Accept": "text/uri-list",
                        "X-Auth-Token": this.token,
                        "Content-Type": "text/uri-list"
            }
            var obj = this;
            return http.Get(url, headers).then((response)=>{
                var body = response.body.toString().split("\n")
                body.shift()
                var computes = []
                for (var i in body) {
                    var compute = body[i]
                    if(extended)
                      computes.push(obj.describe(compute.substring(compute.lastIndexOf("/"))))
                    else
                      computes.push(Promise.resolve(compute.lastIndexOf("/")))
                }
                return Promise.settle(computes).then((results)=>{
                  var computers = []
                  for(var i in results){
                      if(results[i]._settledValue.id !== undefined)
                        computers.push(results[i]._settledValue)
                  }
                  return computers

                })
            })
        }


        /**
        * Returns the description of a computer
        *@method describe
        *@param {string} id Identifier of the computer
        *@return {Promise} Computer description
        */
        describe(id: string) {
            var url = this.url + "/compute/" + id
            var headers = {
                "Accept": "application/occi+json",
                "X-Auth-Token": this.token,
                "Content-Type": "application/occi+json"
            }

            return http.Get(url, headers).then(function (response) {
                var body = response.body
                body = JSON.parse(body.toString())
                var compute = new models.Compute(body.attributes['occi.core.id'])
                compute.hostname = body.attributes['occi.compute.hostname']
                compute.cores = body.attributes['occi.compute.cores']
                compute.memory = body.attributes['occi.compute.memory']

                if(body.attributes['org.openstack.compute.state'] !== undefined)
                {
                  switch(body.attributes['org.openstack.compute.state'].toUpperCase()){
                    case 'ERROR':
                      compute.state = State.ERROR
                      break
                    case 'BUILDING':
                      compute.state = State.CREATING
                      break
                    case 'ACTIVE':
                      compute.state = State.CREATING
                      break
                    case 'STOPPED':
                      compute.state = State.ENDED
                      break
                    case 'SUSPENDED':
                      compute.state = State.ENDED
                      break
                    case 'PAUSED':
                      compute.state = State.ENDED
                      break
                    case 'DELETED':
                      compute.state = State.ENDED
                      break
                  }
                }

                if(compute.state === undefined){
                  compute.state = body.attributes['occi.compute.state'].toUpperCase()
                  if(compute.state == 'ACTIVE')
                      compute.state = State.CREATING
                  if(compute.state == 'INACTIVE')
                      compute.state = State.CREATING
                }
                var ips = []
                for (var i in body.links) {
                    if (body.links[i].kind.location == '/network/interface/')
                        ips.push(body.links[i].attributes['occi.networkinterface.address'])
                }
                compute.ips = ips
                return compute
            });
        }

        /**
        * Create a new computer
        *@method create
        *@param {string} title Computer title.
        *@param {Object} params Parameters to create the computer
        *@param {string} params.image Image to use to create the computer
        *@param {string} params.flavor Flavor to use to create the computer
        *@param {Array} [params.credentials] Array of public key to use to connect to the computer
        *@param {string} [params.user_data] Path to the user data. This file will be use to contextualice the computer.
        *@return {Promise} Resolve with the computer description if the creation is succesfull
        **/
        create(title: string, params: { image: string; flavor: string; credentials: Array<any>; user_data: string }) {
            var url = this.url + "/compute/"
            var cert = this.cert;
            var data = 'Category: compute;scheme="http://schemas.ogf.org/occi/infrastructure#";class="kind";location="/compute/";title="Compute Resource"\n';
            data += 'Category: '+params.image+';scheme="http://schemas.openstack.org/template/os#";class="mixin";location="/'+params.image+'/"\n';
            data += 'Category: '+params.flavor+';scheme="http://schemas.openstack.org/template/resource#";class="mixin";location="/'+params.flavor+'/";title="Flavor: '+params.flavor+'"\n';
            data += 'Category: user_data;scheme="http://schemas.openstack.org/compute/instance#";class="mixin";location="/mixin/user_data/";title="OS contextualization mixin"\n';
            data += 'X-OCCI-Attribute: occi.core.id="' + uuid.v4() + '"\n';
            data += 'X-OCCI-Attribute: occi.core.title="' + title + '"\n';
            data += 'X-OCCI-Attribute: occi.compute.hostname="' + title + '"\n';
            if (params.user_data != undefined) {
                data += 'X-OCCI-Attribute: org.openstack.compute.user_data="' + params.user_data + '"'
            }

            var headers = {
                "Accept": "text/plain,text/occi",
                "X-Auth-Token": this.token,
                "Content-Type": "text/plain,text/occi",
                "Connection": "close"
            }

            var obj = this
            return http.Post(url, data, headers, fs.readFileSync(cert))
              .then((result)=>{
                  var action = ()=>{
                    var id = result.headers['Location']
                    if(id == undefined)
                        id = result.headers['location']
                    if(id === undefined)
                    {
                      logger.warn("Authenticated site, reSubmit server")
                      return obj.create(title, params)
                    }else{
                      logger.debug("Created server: "+id)
                      return Promise.resolve(obj.describe(id.substring(id.lastIndexOf("/"))))
                    }
                  }

                  if('www-authenticate' in result.headers){
                    console.log("reautentica")
                    return obj.renewToken().then(action)
                  }
                  return action()
              }).catch(function (result) {
                    if(result.body === undefined){
                      logger.error("Body is undefined")
                      logger.debug(JSON.stringify(result))
                      process.exit()
                      return Promise.reject("Body is undefined")
                    }else{
                      logger.error(result.body.toString());
                      return Promise.reject(result.body.toString())
                    }
              })
        }

        /**
        *Delete a computer
        *@method delete
        *@param {string} id Computer identifier
        */
        delete(server:any):any {
            var url = this.url + "/compute/" + server.id
            var cert = this.cert
            var headers = {
                "Accept": "application/occi+json",
                "X-Auth-Token": this.token,
                "Content-Type": "application/occi+json"
            }
            logger.debug("Deleting server "+ server.id)
            return http.Delete(url, headers, fs.readFileSync(cert))
              .then((body)=> {
                    logger.debug("Deleted server "+ server.id)
                    return Promise.resolve(server)
              }).catch((err)=>{
                    if (err == 404) {
                        return Promise.reject("The resource " + server.id + " not exists");
                    }
            });
        }

        /**
        *Links a network to a computer
        *@method linkNetwork
        *@param {string} id Computer identifier
        */
        linkNetwork(id:string){
            var url = this.url + "/network/interface";
            var cert = this.cert;

            var data = 'Category: networkinterface;scheme="http://schemas.ogf.org/occi/infrastructure#";class="kind";';
            data += 'location="/link/networkinterface/";title="networkinterface link"\n';
            data += 'X-OCCI-Attribute: occi.core.id="'+ uuid.v4() +'"\n';
            data += 'X-OCCI-Attribute: occi.core.target="/network/public"\n';
            data += 'X-OCCI-Attribute: occi.core.source="/compute/6691a2c6-186f-42ca-b67f-bf9809e02660"';

            var headers = {
                'Accept':'text/plain text/occi',
                'X-Auth-Token': this.token,
                'Content-Type':'text/plain,text/occi',
                'Connection':'close',
            };

            var obj = this;
            return new Promise(function (resolve, reject) {
                http.Post(url, data, headers, fs.readFileSync(cert)).then(function (result) {
                    var id = result.headers['Location']
                    if(id == undefined)
                        id = result.headers['location']
                    logger.debug("Created network: "+ id)
                    resolve(id);
                }).catch(function (result) {
                        logger.error(result);
                });
            });

        }

        private renewToken():any{
          this.client.getKeystoneUrl().then((url)=>{
              if(url != undefined){
                return new openStack.Openstack(url, this.client.cert).auth()
                    .then((token)=>{
                          obj.token = token
                          console.log("Porque?")
                          console.log(obj.token)
                          return Promise.resolve()
                    }).catch((err)=>{
                          logger.warn(err)
                          return Promise.reject(err)
                      });
                  }else{
                      return Promise.resolve();
                  }
              });
        }
    }
}
