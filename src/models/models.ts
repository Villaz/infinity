export class Compute {
    id: string
    hostname: string
    cores: number
    memory: number
    state: string
    disk:any
    ips: Array<string>
    site: string

    constructor(id: string) {
        this.id = id
    }
}


export class MachineConfig{

}

export class MachineImage{

}


export interface Template{
    id
    machineConfig
    machineImage
    credentials?
}

export class MachineTemplate{

    id: string
    resource: string
    machine_config
    machine_image
    credentials
    user_data
    network

    constructor(id:string);
    constructor(id:Template);
    constructor(id:any){
        this.id = id
        this.resource = 'http://schemas.dmtf.org/cimi/1/MachineTemplate'
        if (typeof id == "object"){
            this.machine_config = id.machineConfig
            this.machine_image = id.machineImage
            this.credentials = id.credentials
            this.id = id.id
        }else{
            this.id = id;
        }
    }


    json(){
      var dict = {'resourceURI':this.resource,
              'machineConfig':{ 'href': this.machine_config.id,
                                'resourceURI':'http://schemas.dmtf.org/cimi/1/MachineConfiguration'
                            },
              'machineImage':{  'href': this.machine_image.id,
                                'resourceURI':'http://schemas.dmtf.org/cimi/1/MachineImage'
                              },
              "networkInterfaces":[{'network':{'href':'https://ec2-54-228-25-65.eu-west-1.compute.amazonaws.com/cxf/iaas/networks/2bdb4c9e-7121-48ba-a8a9-231f38336f66'}},
                                   {"network":{"networkType":"PUBLIC"}}]
              }

      if(this.network)
         dict['networkInterfaces'] = [this.network.json(),{"network":{"networkType":"PUBLIC"}}]
      if(this.credentials)
         dict['credentials'] = this.credentials
      if(this.user_data)
         dict['userData'] = this.user_data
      return dict
    }
}
