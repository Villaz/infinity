///<reference path='../typings/node/node.d.ts' />
var program = require('commander');
var moment = require("moment");
var MongoClient = require('mongodb').MongoClient;
var Promise = require('bluebird');
var mubsub = require('mubsub');

program
    .version('0.0.1')
    .option('-i --id <identifier>', 'Machine Identifier')
    .option('-s --site <site>', 'Site where is deployed the VM')
    .option('-t --type <type>', 'Type update', /^(boot|start|heartbeat|dependencies|end)$/i, 'heartbeat')
    .parse(process.argv);


function updateField(db: any, id: string, document, state:string) {
    return new Promise(function (resolve, reject) {
        var collection = db.collection('servers');
        collection.update({ 'hostname': id },
            { $set: document }, function (err, result) {
                if (err){
                    console.log(err);
                    reject(err);
                }
                resolve({db:db ,id:id, state:state});
            });
    });
}

function addToCap(dict)
{
    if(dict.state !== 'end'){
      return Promise.resolve();
    }
    var client = mubsub(dict.db);
    var channel = client.channel("ended_server");

    return new Promise((resolve, reject) => {
      channel.on('error', reject);
      client.on('error', reject);
      channel.publish(program.site, { hostname:dict.id },(err, doc)=>{
        resolve()
      });
    });

}


function updateDB(id:string , type:string){
    var dateCreation = moment.unix(parseInt(id.substr(id.indexOf("-") + 1)));
    var now = moment();
    var diffDates = now.diff(dateCreation, 'seconds')

    var document = {}
    switch(program.type){
        case 'boot':
            document = {'boot':diffDates , 'state':'STARTED'}
            break;
        case 'dependencies':
            document = {'dependencies':diffDates}
            break;
        case 'start':
            document = {'start':diffDates , 'state':'RUNNING'}
            break;
        case 'end':
            document = {'end':diffDates, 'state':'ENDED'}
            break;
        case 'heartbeat':
            document = {'heartbeat':moment().unix()}
            break;
    }

    var url = 'mongodb://luis:Espronceda@ds047911.mongolab.com:47911/infinity';
    MongoClient.connect(url, function(err, db){
        if (err)
            process.exit(-1);
        updateField(db, id, document, program.type).then(addToCap).finally(function () { process.exit(0); });
    });
}

if (program.id == undefined) {
    console.error("The id is required");
    process.exit(-1);
}

updateDB(program.id, program.type);
