var elasticsearch = require('elasticsearch')
var moment = require('moment')
var metrics = require('./metrics')
var Promise = require('bluebird')
var program = require('commander')
var os = require('os')

program
    .version('0.0.1')
    .option('-e, --server <server>', 'Set elasticSearch server')
    .option('-s, --site <site>', 'Set the site')
    .parse(process.argv)

var client = new elasticsearch.Client({
  host: program.server,
  log: 'trace'
})

Promise.props({
  load:metrics.getLoad(),
  cpus:metrics.getCpus(),
  memory:metrics.getMemory(),
  disk:metrics.getDisk()
}).then((result)=>{
  client.create({index:'servers',
                type:program.site,
                body:{hostname:os.hostname(),
                load:{cpus:result.cpus.summary.total,
                      nodes:1,
                      '1-min':result.load['1-min']},
                memory:result.memory,
                cpu:{user:result.cpus.summary.user,
                     nice:result.cpus.summary.nice,
                     sys:result.cpus.summary.sys,
                     idle:result.cpus.summary.idle},
                disk:result.disk,
                '@timestamp':moment().toISOString()}})
                .then(function(b){console.log(b);process.exit(0)})
                .catch(function(c){console.log(c);process.exit(0)})
})
