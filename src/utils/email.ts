var Promise = require("bluebird");

export class Mail {

    params: {}

    constructor(params: { user: string; password: string; }) {
        this.params = params;

    }

    public send(from, to, subject, text) {
        var https = require("https");
        var querystring = require("querystring");

        var postData = querystring.stringify({
            api_user: this.params['user'],
            api_key: this.params['password'],
            to: to,
            from: from,
            subject: subject,
            text: text
        });
        console.log(postData);
        var options = {
            hostname: 'api.sendgrid.com',
            path: '/api/mail.send.json',
            port: 443,
            method: 'POST',
            headers: {
                'Content-Length': postData.length,
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        };

        return new Promise(function (resolve, reject) {
            var req = https.request(options, function (res) {
                var buffer = undefined
                res.on("data", function (chunk) {
                    if (buffer == undefined)
                        buffer = chunk
                else
                        buffer = Buffer.concat([buffer, chunk])
                });

                res.on("end", function () {
                    if (JSON.parse(buffer)["message"] == "sucess")
                        resolve()
                    else
                        reject(buffer.toString())
                });
                res.on("error", function (error) {
                    reject(error)
                });
            });
            req.write(postData);
            req.end();
        });
    }
}

var ops = { user: "Villaz", password:"Espronceda1985"}
var m = new Mail(ops);
m.send("villazonpersonal@gmail.com", "villazonpersonal@gmail.com", "test", "test").then(function (v) {
    console.log(v.toString());
});