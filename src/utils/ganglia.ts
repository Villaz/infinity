///<reference path='../../typings/node/node.d.ts' />

var querystring = require('querystring')
var http = require('./http.js')
var Promise = require('bluebird')
var Rule = require('./rules.js');

export class Ganglia{

    url:string

    constructor(url:string){
        this.url = url + '/graph.php'
    }

    public info(params){
        return Promise.props({
                'cpu': this.cpu_info(params),
                'memory': this.memory_info(params),
                'load': this.load_info(params),
                'network':this.network_info(params),
                'disk': this.disk_info(params)
            });
    }

    public load_info(params){
      return this.call(this.generateParams('load_one','load_report', params))
    }

    public cpu_info(params){
        return this.call(this.generateParams('load_one','cpu_report', params))
    }

    public memory_info(params){
        return this.call(this.generateParams('load_one','mem_report', params))
    }

    public network_info(params){
        return this.call(this.generateParams('load_one','network_report', params))
    }

    public disk_info(params){
        return Promise.props({
                                free:this.disk_free(params),
                                total:this.total_disk(params),
                                used: this.max_disk(params)
                            })
    }

    private disk_free(params){
        return this.call(this.generateParams('load_one','disk_free', params))
    }

    private total_disk(params){
        return this.call(this.generateParams('load_one','total_disk', params))
    }

    private max_disk(params){
        return this.call(this.generateParams('load_one','part_max_used', params))
    }


    private call(params){
        var obj = this
        return http.Get(this.url+'?'+params).then(function(values){
            try{
                values = JSON.parse(values.body.toString());
            }catch(ex){
                return Promise.resolve(undefined)
            }
            var result = {}
            if(!values)
                return Promise.resolve(undefined)

            return Promise.resolve(values).each(function(value){
              var metricName = value['metric_name'].replace('\\g','').replace("1-min","min").toLowerCase().trim()
              var datapoints = []
              for(var i in value['datapoints'])
              {
                if(value['datapoints'][i][0] !== 'NaN')
                  datapoints.push(value['datapoints'][i])
              }
              result[metricName] = datapoints
            }).then(function(){
              return result
            })
        })
    }

    private generateParams(m, g , params){
      var qParams = {
                    m: m,
                    g: g,
                    c: params.cluster,
                    json: 1
                    }
      if(params.hostname)
        qParams['h'] = params.hostname
      if(params.r === undefined)
      {
        qParams['cs'] = params.start
        qParams['ce'] = params.end
      }else{
        qParams['r'] = params.r
      }
      return querystring.stringify(qParams)
    }

    private avg(values:Array<any>){
        var sum = 0
        var count = 0
        for(var i in values){
            if(values[i][0] != undefined && values[i][0] != 'NaN'){
                sum +=values[i][0]
                count++
            }
        }
        return sum/count
    }

}
