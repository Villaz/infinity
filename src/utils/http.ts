///<reference path='../../typings/node/node.d.ts' />
var http = require("http")
var https = require("https")
var Promise = require("bluebird")

function _getUrlParts(url: string) {
    var protocol = url.substr(0, url.indexOf(":"));
    var hostname = undefined
    var port = 80
    var path = undefined

    url = url.substr(url.indexOf("/") + 2);
    if(url.indexOf(':') < 0 ){
        if(url.indexOf('/') > 0){
            hostname = url.substr(0, url.indexOf("/"));
            url = url.substr(url.indexOf("/") + 1);
        }else{
            hostname = url
            url = ""
        }
    }else{
        hostname = url.substr(0, url.indexOf(":"));
        url = url.substr(url.indexOf(":") + 1);
        if (url.indexOf("/") < 0)
            port = parseInt(url);
        else {
            port = parseInt(url.substr(0, url.indexOf("/")));
            url = url.substr(url.indexOf("/") + 1);
        }
    }

    if (url.length > 0 && isNaN(Number(url)))
        path = "/" + url;
    else
        path = "/";

    return {
        protocol: protocol,
        url: url,
        host: hostname,
        port: port,
        path: path
    }
}

export function Head(url: string, cert=undefined) {
    var parts = _getUrlParts(url)
    var options = {
        port: parts.port,
        host: parts.host,
        path: parts.path,
        method: "HEAD",
        rejectUnauthorized: false,
    }

    if (cert != undefined) {
        options['cert'] = cert;
        options['key'] = cert;
    }

    return new Promise(function (resolve, reject) {
        if (parts.protocol == 'http') {
            var req = http.request(options, function (req) {
                var headers = req.headers
                resolve(headers)
            });
        } else {
            if (options.port == 80)
                options.port = 443
                var req = https.request(options, function (req) {
                    var headers = req.headers
                    resolve(headers)
               });

            }
        req.on('error',function(error)
            {
                if(error.toString().indexOf("sslv3 alert certificate expired") >= 0)
                reject("sslv3 certificate expired");
            });
        req.end()
    });
}


export function Get(url, headers: any= undefined, cert:any=undefined, key:any=undefined) {
    var parts = _getUrlParts(url)
    var options = {
        port: parts.port,
        host: parts.host,
        path: parts.path,
        method: "GET",
        headers: headers,
        rejectUnauthorized: false,
    }

    if (cert !== undefined) {
        options['cert'] = cert
        options['key'] = cert
        }

    if (key !== undefined){
      options['key'] = key
    }

    if (options.headers !== undefined && 'Content-Length' in options.headers){
        delete options.headers['Content-Length']
    }
    return new Promise(function (resolve, reject) {
        var processResponse = function (req) {
            var buffer = undefined
            req.on("data", function (chunk) {
                if (buffer == undefined)
                    buffer = chunk
                else
                    buffer = Buffer.concat([buffer, chunk])
            });

            req.on("end", function () {
                if (req.statusCode == 502)
                    reject({ error: req.statusCode, headers: req.headers, status: req.statusCode })
                else
                    resolve({ body: buffer, headers: req.headers, status: req.statusCode })
                });
            req.on("error", function (error) {
                reject({ body: buffer, headers: req.header, status: req.statusCodes })
            });
        }

        if (parts.protocol == 'http') {
            var req = http.get(options, function (req) {
                processResponse(req)
            })
        } else {
            if(options.port == 80)
                options.port = 443
            var req = https.get(options, function (req) {
                processResponse(req)
            })
        }

        req.on('error',function(error){
            if(error.toString().indexOf("sslv3 alert certificate expired") >= 0)
                reject("sslv3 certificate expired");
        });
        req.on('timeout', function(error){
          try{
            req.end()
          }catch(e){}
          reject("Timeout: host: " + options.host +" path:"+ options.path)
        });

        req.setTimeout(60 * 1000);
        req.end()
    });
}


export function Post(url, data, headers: any= undefined, cert: any= undefined, key: any = undefined) {
    var parts = _getUrlParts(url)
    var options = {
        port: parts.port,
        host: parts.host,
        path: parts.path,
        method: "POST",
        headers: headers,
        rejectUnauthorized: false,
        //requestCert: true,
    }

    if (cert !== undefined) {
        options['cert'] = cert
        options['key'] = cert
        }

    if (key !== undefined){
      options['key'] = key
    }
    if (data != undefined && (typeof data == "string"))
        options.headers['Content-Length'] = data.toString().length
    options.headers['Connection'] = 'keep-alive'
    Promise.longStackTraces()
    return new Promise(function (resolve, reject) {

        var processResponse = function (req) {

            var buffer = undefined
            req.on("data", function (chunk) {
                if (buffer == undefined)
                    buffer = chunk
                else
                    buffer = Buffer.concat([buffer, chunk])
            });

            req.on("end", function () {
                if (req.statusCode == 404 || req.statusCode == 400) {
                    reject({ body: buffer, headers: req.headers, status: req.statusCode });
                }else {
                    resolve({ body: buffer, headers: req.headers, status: req.statusCode });
                }
            });

            req.on("error", function (error) {
                reject({ body: error, headers: req.headers, status: req.statusCode })
            });
        }

        if (parts.protocol == 'http') {
            var req = http.request(options, function (req) {
                processResponse(req)
            });
        } else {
            if (options.port == 80)
                options.port = 443
            var req = https.request(options, function (req) {
                processResponse(req)
            });
        }

        if(data != undefined && (typeof data != "string"))
            req.write(JSON.stringify(data))
        else if (data != undefined)
            req.write(data)

        req.on('error',function(error){
            if(error.toString().indexOf("sslv3 alert certificate expired") >= 0)
                reject("sslv3 certificate expired")
            else reject(error)
        });

        req.on('timeout', function(error){
          try{
            req.end()
          }catch(e){}
          reject("Timeout")
        })

        req.setTimeout(40 * 1000);
        req.end()
    });
}

export function Put(url, data, headers: any= undefined, cert: any= undefined, key: any = undefined) {
    var parts = _getUrlParts(url)
    var options = {
        port: parts.port,
        host: parts.host,
        path: parts.path,
        method: "PUT",
        headers: headers,
        rejectUnauthorized: false,
        //requestCert: true,
    }

    if (cert !== undefined) {
        options['cert'] = cert
        options['key'] = cert
        }

    if (key !== undefined){
      options['key'] = key
    }
    if (data != undefined && (typeof data == "string"))
        options.headers['Content-Length'] = data.toString().length
    options.headers['Connection'] = 'keep-alive'

    return new Promise(function (resolve, reject) {

        var processResponse = function (req) {

            var buffer = undefined
            req.on("data", function (chunk) {
                if (buffer == undefined)
                    buffer = chunk
                else
                    buffer = Buffer.concat([buffer, chunk])
            });

            req.on("end", function () {
                if (req.statusCode == 404 || req.statusCode == 400) {
                    reject({ body: buffer, headers: req.headers, status: req.statusCode });
                }else {
                    resolve({ body: buffer, headers: req.headers, status: req.statusCode });
                }
            });

            req.on("error", function (error) {
                reject({ body: error, headers: req.headers, status: req.statusCode })
            });
        }

        if (parts.protocol == 'http') {
            var req = http.request(options, function (req) {
                processResponse(req)
            });
        } else {
            if (options.port == 80)
                options.port = 443
            var req = https.request(options, function (req) {
                processResponse(req)
            });
        }

        if(data != undefined && (typeof data != "string"))
            req.write(JSON.stringify(data))
        else if (data != undefined)
            req.write(data)

        req.on('error',function(error){
            if(error.toString().indexOf("sslv3 alert certificate expired") >= 0)
                reject("sslv3 certificate expired");
        });

        req.on('timeout', function(error){
          try{
            req.end();
          }catch(e){};
          reject("Timeout");
        });

        req.setTimeout(40 * 1000);
        req.end()
    });
}


export function Delete(url, headers: any= undefined, cert: any= undefined, key: any= undefined) {
    var parts = _getUrlParts(url)
    var options = {
        port: parts.port,
        host: parts.host,
        path: parts.path,
        method: "DELETE",
        headers: headers,
        rejectUnauthorized: false,
        //requestCert: true,
    }

    if (cert !== undefined) {
        options['cert'] = cert
        options['key'] = cert
    }

    if(key !== undefined){
      options['key'] = key
    }

    return new Promise(function (resolve, reject) {
        var processResponse = function (req) {
            if (req.statusCode == 404) {
                reject(404);
                return;
            }
            var buffer = undefined
            req.on("data", function (chunk) {
                if (buffer == undefined)
                    buffer = chunk
                else
                    buffer = Buffer.concat([buffer, chunk])
            });
            req.on("end", function () {
                resolve(buffer)
            });
            req.on("error", function (error) {
                reject(error)
            });
        }

        if (parts.protocol == 'http') {
            var req = http.request(options, function (req) {
                processResponse(req)
            });
        } else {
            if (options.port == 80)
                options.port = 443
            var req = https.request(options, function (req) {
                processResponse(req)
            });
        }

        req.on('error',function(error){
            if(error.toString().indexOf("sslv3 alert certificate expired") >= 0)
                reject("sslv3 certificate expired");
        });

        req.on('timeout', function(error){
          try{
            req.end();
          }catch(e){};
          reject("Timeout");
        });

        req.setTimeout(40 * 1000);

        req.end()
    });
}
