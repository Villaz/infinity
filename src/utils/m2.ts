///<reference path='../../typings/node/node.d.ts' />
var mu = require('mu2');
var Promise = require('bluebird')

export function resolveUserData(file, params){
	mu.root =  './'
	return new Promise(function (resolve, reject) {
        var user_data = undefined;
        mu.compileAndRender(file, params)
            .on('data', function (data) {
                if (user_data == undefined)
                    user_data = data
                    else
                        user_data += data;
            }).on('end', function () {
                resolve(new Buffer(user_data).toString('base64'));
            });
	});

}
