///<reference path='../../typings/node/node.d.ts' />
var os = require("os")
var fs = require("fs")
var Promise = require("bluebird")

export function getLoad(){
  var load = os.loadavg()
  return Promise.resolve({'1-min':load[0],'5-min':load[1],'15-min':load[2]})
}

export function getCpus(){
  var cpus = []
  var total:number=0
  var dict = {summary:{user:0,nice:0,sys:0,idle:0,total:0},cpus:[]}
  return Promise.resolve(os.cpus()).each((cpu)=>{
    var total:number = 0
    total = cpu.times.user + cpu.times.nice + cpu.times.sys + cpu.times.idle
    cpus.push({model:cpu.model,
               user:(cpu.times.user/total)*100,
               nice:(cpu.times.nice/total)*100,
               sys:(cpu.times.sys/total)*100,
               idle:(cpu.times.idle/total)*100})
    dict.summary.user += (cpu.times.user/total)*100
    dict.summary.nice += (cpu.times.nice/total)*100
    dict.summary.sys += (cpu.times.sys/total)*100
    dict.summary.idle += (cpu.times.idle/total)*100
  }).then(()=>{
    dict.summary.total = cpus.length
    dict.summary.user /= cpus.length
    dict.summary.nice /= cpus.length
    dict.summary.sys /= cpus.length
    dict.summary.idle /= cpus.length
    dict.cpus=cpus
    return dict})
}

export function getMemory(){
  var params = fs.readFileSync("/proc/meminfo").toString().split("\n")
  var total = 0
  var free = 0
  var cached = 0
  var swapFree = 0
  var swapTotal = 0
  var buffer = 0
  return Promise.resolve(params).each((param)=>{
    if(param.indexOf("MemTotal") >= 0){
      total = parseInt(param.substr(param.indexOf(":")+1).replace("kB","").trim())/1024
    }else if(param.indexOf("MemFree") >= 0){
      free = parseInt(param.substr(param.indexOf(":")+1).replace("kB","").trim())/1024
    }else if(param.indexOf("Cached") >= 0){
      cached = parseInt(param.substr(param.indexOf(":")+1).replace("kB","").trim())/1024
    }else if(param.indexOf("SwapFree") >= 0){
      swapFree = parseInt(param.substr(param.indexOf(":")+1).replace("kB","").trim())/1024
    }else if(param.indexOf("SwapTotal") >= 0){
      swapTotal = parseInt(param.substr(param.indexOf(":")+1).replace("kB","").trim())/1024
    }else if(param.indexOf("Buffers") >= 0){
      buffer = parseInt(param.substr(param.indexOf(":")+1).replace("kB","").trim())/1024
    }
  }).then(()=>{return {total:total,use:total-free, free:free, cached:cached, swap:swapTotal-swapFree, buffer:buffer}})
}

export function getDisk(){
  var exec = require('child_process').exec

  return new Promise((resolve,reject)=>{
    var child = exec('df -k /',function (error, stdout, stderr) {
      if(error !== null)
      {
        reject(error)
        return
      }
      var values =stdout.split("\n")[1].split(" ")
      values.shift()
      var counter = 0
      var value = {used:undefined, total:undefined, free:undefined}
      for(var i in values){
        if(values[i] !== '')
          if(value.total === undefined)
            value.total = values[i]/(1024*1024)
          else if(value.total !== undefined && value.used === undefined)
            value.used = values[i]/(1024*1024)
          else{
            value.free = values[i]/(1024*1024)
            break
          }
      }
      resolve(value)
    })
  })
}
