///<reference path='../../typings/node/node.d.ts' />
var http = require("http");
var Promise = require("bluebird")
var nodemailer = require('nodemailer');
var plotly = require('plotly')('Villaz','70k33hwv1w');
var fs = require('fs')

function getError(site, host){
	var options = {
		host:'pandamon.cern.ch',
		path:'/jobinfo?hours=24&computingSite='+site+'&modificationHost=*'+host+'&days=0&_get=json',
		port:80,
		headers:{'Accept':'application/json, text/javascript, */*; q=0.01'
			,'Content-Encoding':'gzip, deflate',
			'X-Requested-With':'XMLHttpRequest','pmMonitor_class':'PandaMonitor','pmMonitor_location':'monitorRoot'},
		method:'GET'
	}

	return new Promise(function(resolve,reject){
		var req = http.get(options,function(res){
			var data = undefined

			res.on('data',function(chunk){
				if(data == undefined)
				data = chunk;
			else
				data = Buffer.concat([data, chunk])
			});
			res.on('end',function(){
				var errorCode = undefined
				var error = JSON.parse(data)['pm'][2]['json']['errorCodes']
				error = error[Object.keys(error)[0]]
				errorCode = Object.keys(error)[0]
				error = error[errorCode][1]
				resolve({'errorCode':errorCode,'message':error,host:host})
			});
		});
	});
}


function processData(sites){
	function processEachSite(site){
		return getError('HELIX_NEBULA_EGI',site['nm']).then(function(error){
			return error;
			});
	}

	function reduce(total, error){
		if(error.errorCode in total)
		{
			total[error.errorCode].count++;
			total[error.errorCode].hosts.push(error.host);
		}else
		{
			total[error.errorCode] = {'count':1,'message':error.message,'hosts':[error.host]}
		}
		total.errors++
		total.perc = total.errors/total.total
		return total
	};
	var errors = Promise.resolve(sites)
		.filter(function(site){
			if(site['st'][7] >= 1)
				return true;
			else
				return false;
	}).map(processEachSite).reduce(reduce,{total:sites.length,errors:0,perc:0})

	var finished = Promise.resolve(sites)
	.filter(function(site){
		if(site['st'][6] >= 1)
		return true;
		else
		return false;
		}).reduce(function(total,site){ return ++total;},0);

	return Promise.all([errors, finished])
}

function getErrors()
{
	var errors = {}

	var options = {
		host:'pandamon.cern.ch',
		path:'/wnlist?site=HELIX_NEBULA_EGI&days=1&jobStatus=wn&_get=json',
		port:80,
		headers:{'Accept':'application/json, text/javascript, */*; q=0.01'
		,'Content-Encoding':'gzip, deflate',
		'X-Requested-With':'XMLHttpRequest','pmMonitor_class':'PandaMonitor','pmMonitor_location':'monitorRoot'},
		method:'GET'
	}

	return new Promise(function(resolve,reject){
		var req = http.get(options,function(res){
			var data = undefined
			res.on('data',function(chunk){
				if(data == undefined)
					data = chunk;
				else
					data = Buffer.concat([data, chunk])
			});

			res.on('end',function(){
				var sites = JSON.parse(data)['pm'][2]['json']['data']['status']['sites'][0]['hosts'];
				resolve(processData(sites));
			});
		});
	});
}

function generateGraph(data){
	var data_image = {
		x:['Total Jobs','Completed Jobs','Failed Jobs'],
		y:[data[0].total,data[1],data[0].errors],
		type: 'bar'
	}
	return new Promise(function(resolve,reject){
		var graphOptions = {filename: "basic-bar", fileopt: "overwrite"};
			plotly.plot(data_image, graphOptions, function (err, msg) {
    		resolve(msg.url+'.png')
		});
	});
}

function generateEmail(data){
	return generateGraph(data).then(function(image){
		var errors = data[0]
		var codes = Object.keys(errors)
		var html = "<html><head></head><body>"
		html += '<h3 align="center">Total Jobs:'+ errors.total +"; Failed:"+ errors.errors +"; Perc Failed:"+ (errors.perc*100).toFixed(2)+"%<br/>"
		html += '<img src="'+image+'">'
		for(var i in codes){
			var code = codes[i]
			if(code != 'total' && code != 'errors' && code != 'perc'){
				html += '<h4 align="center">'+errors[code].message+'</h4>'
				html += '<h5 align="center">Total:'+errors[code].count+'</h5>'
				for(var j in errors[code].hosts){
					var host = errors[code].hosts[j];
					html += '<a href="http://pandamon.cern.ch/jobinfo?hours=24&computingSite=HELIX_NEBULA_EGI&modificationHost=*'+host+'&days=0">'+ host +'</a>  (<a href="http://vcycle-manager-lv/'+host+'">vcycle</a>)</br>'
				}
			}
		}
		html += '</body></html>'
		return html
	});

}


function sendEmail(html){
	fs.writeFileSync('text.html',html)
	var nodemailer = require('nodemailer');
	var smtpTransport = require('nodemailer-smtp-transport');
	// create reusable transporter object using SMTP transport
	var transporter =  nodemailer.createTransport(smtpTransport({
    	host: 'smtp.cern.ch',
    	port: 587,
    	auth: {
        	user: 'luis.villazon.esteban@cern.ch',
        	pass: 'Espronceda1985'
    	}
	}));

	// setup e-mail data with unicode symbols
	var mailOptions = {
    	from: 'luis.villazon.esteban@cern.ch', // sender address
    	to: 'luis.villazon.esteban@cern.ch', // list of receivers
    	subject: 'Failures EGI', // Subject line
    	html: html // html body
	};

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
    	if(error){
        	console.log(error);
    	}else{
        	console.log('Message sent: ' + info.response);
    	}
	});
}

getErrors().then(generateEmail).then(sendEmail)
/*getErrors().then(function(errors){
	//console.log(errors)

	sendEmail(generateEmail(errors))
});*/
