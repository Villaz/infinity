var Promise = require("bluebird");

export class Rule {

    rule: string

     constructor(rule: string) {  
        this.rule = rule
    }

    eval(params: {}): boolean {
        var obj = this
        var map = {}
        var keys = Object.keys(params)
        var evaluation = this.rule
        for (var i in keys)
        {
            var key = keys[i];
            var keys2 = Object.keys(params[key])
            for (var k in keys2)
            {
                var key2 = keys2[k]
                map[key + "." + key2] = params[key][key2]
            }
        }

        keys = Object.keys(map)
        for (var i in keys)
        {
            evaluation = evaluation.replace(keys[i], map[keys[i]]);
        }
        return eval(evaluation)
    }

    private getKey(param,pos)
    {
        return Object.keys(param);
    }
}