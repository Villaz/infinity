///<reference path='../../typings/node/node.d.ts' />
var util = require('util')
var winston = require('winston')
var moment = require("moment")

  var GrayLogger = winston.transports.CustomLogger = function (options) {
    //
    // Name this logger
    //
    this.name = 'grayLogger';

    //
    // Set the level from your options
    //
    this.level = options.level || 'info';

    //
    // Configure your storage backing as you see fit
    //
    this.port = options.port
    this.host = options.host
    this.label = options.label
  };

  //
  // Inherit from `winston.Transport` so you can take advantage
  // of the base functionality and `.handleExceptions()`.
  //
  util.inherits(GrayLogger, winston.Transport);

  GrayLogger.prototype.log = function (level, msg, meta, callback) {
    var message = {
      version: "1.1",
      host: this.label,
      short_message: msg,
      timestamp: moment().unix(),
      level: 1,
    }
    if(level === 'silly')
      message.level = 8
    else if(level === "debug")
      message.level = 7
    else if(level === 'verbose')
      message.level = 6
    else if(level === "info")
      message.level = 5
    else if(level === "warn")
      message.level = 4
    else if(level === "error")
      message.level = 3


    var dgram = require("dgram")
    var client = dgram.createSocket("udp4")
    var buffer = new Buffer(JSON.stringify(message))
      client.send(buffer, 0, buffer.length, this.port,this.host, function (err) {
          client.close();
      })

  }

module.exports = {GrayLogger:GrayLogger}
