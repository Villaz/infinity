var should = require('should');
var Promise = require("bluebird");
var Create = require("../../src/conditions/create").CreateComputer;
var db = require("../../src/db/db").DB;
var moment = require("moment")

function error(err) {
    console.log(err)
    err.should.be.empty;
};

describe('Create computer tests', function() {

    var obj = {};
    var client;
    this.timeout(1000);

    var connector = {
        compute: {
            delete: function (i) { return Promise.resolve(i); }
        }
    };

    var config = {
      heartbeat:600,
      start_time:600,
      site:'TEST'
    }

    before(function(done) {
        this.timeout(30000);
        client = new db.MongoDB('ds062797.mongolab.com:62797/infinity_test', 'luis', 'Espronceda');
        client.connect()
        .then(function(){
          obj.db = client
          obj.info = {}
          done()
        })
        .catch(error);
    });

    afterEach(function(done){
      client.deleteAll().then(done).catch(error);
    });


    describe('checkOneMachineAndIsRunning',function(){
      it('No machines STARTED or CREATED', function( done ){
        Create.checkOneMachineAndIsRunning(obj).then(function(message){
          should.not.exist(message)
          done()
        })
      })


      it('One machine has state STARTED', function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'1',hostname:'1',state:'ENDED', createdTime:moment().subtract(500,'seconds').unix()}
                  ]
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            Create.checkOneMachineAndIsRunning(obj).catch(function(message){
              message.should.match(/^Server running but not started to work/g)
              done()
            })
          })
        })
      })


      it('One machine has state STOPPED', function( done ){
        var list = [{id:'1',hostname:'1',state:'STOPPED', createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'1',hostname:'1',state:'ENDED', createdTime:moment().subtract(500,'seconds').unix()}
                  ]
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            Create.checkOneMachineAndIsRunning(obj).catch(function(message){
              message.should.match(/^Server running but not started to work/g)
              done()
            })
          })
        })
      })


      it('More than one machine has state STARTED', function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'1',hostname:'1',state:'STARTED', createdTime:moment().subtract(500,'seconds').unix()}
                  ]
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            Create.checkOneMachineAndIsRunning(obj).then(function(message){
              should.not.exist(message)
              done()
            })
          })
        })
      })


      it('More than one machine has state STOPPED', function( done ){
        var list = [{id:'1',hostname:'1',state:'STOPPED', createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'1',hostname:'1',state:'STOPPED', createdTime:moment().subtract(500,'seconds').unix()}
                  ]
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            Create.checkOneMachineAndIsRunning(obj).then(function(message){
              should.not.exist(message)
              done()
            })
          })
        })
      })


      it('More than one machine has state STARTED or STOPPED', function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'1',hostname:'1',state:'STOPPED', createdTime:moment().subtract(500,'seconds').unix()}
                  ]
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            Create.checkOneMachineAndIsRunning(obj).then(function(message){
              should.not.exist(message)
              done()
            })
          })
        })
      })


      it('Machine with start parameter', function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', start:123, createdTime:moment().subtract(500,'seconds').unix()}]
        client.insertServer(list[0]).then(function(){
          Create.checkOneMachineAndIsRunning(obj).then(function(message){
            message.should.match(/^Machines executing/g)
            done()
          })
        })
      })
    })

    describe('checkRunningComputersHigherThanLimit', function( done ){

      it("Servers equal than limit", function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', start:123, createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'2',hostname:'2',state:'STOPPED', start:123, createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'3',hostname:'3',state:'ENDED', start:123, createdTime:moment().subtract(500,'seconds').unix()}]
        obj.info.max_machines = 3
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            client.insertServer(list[2]).then(function(){
              Create.checkRunningComputersHigherThanLimit(obj).catch(function(){
                done()
              })
            })
          })
        })
      })

      it("Servers higher than limit", function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', start:123, createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'2',hostname:'2',state:'STOPPED', start:123, createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'3',hostname:'3',state:'ENDED', start:123, createdTime:moment().subtract(500,'seconds').unix()}]
        obj.info.max_machines = 2
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            client.insertServer(list[2]).then(function(){
              Create.checkRunningComputersHigherThanLimit(obj).catch(function(){
                done()
              })
            })
          })
        })
      })

      it("Servers lower than limit", function( done ){
        var list = [{id:'1',hostname:'1',state:'STARTED', start:123, createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'2',hostname:'2',state:'STOPPED', start:123, createdTime:moment().subtract(500,'seconds').unix()},
                    {id:'3',hostname:'3',state:'ENDED', start:123, createdTime:moment().subtract(500,'seconds').unix()}]
        obj.info.max_machines = 4
        client.insertServer(list[0]).then(function(){
          client.insertServer(list[1]).then(function(){
            client.insertServer(list[2]).then(function(){
              Create.checkRunningComputersHigherThanLimit(obj).then(function(){
                done()
              })
            })
          })
        })
      })
    })


  describe('checkComputersCreatedThisRound',function(){

    it("Lower than limit", function( done ){
      obj.created = 1
      obj.maxNumCreations = 2
      Create.checkComputersCreatedThisRound(obj).then(function(){done()})
    })


    it("Equal than limit", function( done ){
      obj.created = 2
      obj.maxNumCreations = 2
      Create.checkComputersCreatedThisRound(obj).catch(function(){done()})
    })


    it("Higher than limit", function( done ){
      obj.created = 3
      obj.maxNumCreations = 2
      Create.checkComputersCreatedThisRound(obj).catch(function(){done()})
    })
  })
})
