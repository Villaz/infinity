﻿var should = require('should');
var Promise = require("bluebird");
var Delete = require("../../src/conditions/delete").DeleteComputer;
var db = require("../../src/db/db").DB;
var moment = require("moment")

function error(err) {
    err.should.be.empty;
};

describe('Delete computer tests', function() {

    var client;
    this.timeout(1000);

    var connector = {
        compute: {
            delete: function (i) { return Promise.resolve(i); }
        }
    };

    var config = {
      heartbeat:600,
      start_time:600,
      name:'TEST'
    }

    before(function(done) {
        this.timeout(30000);
        client = new db.MongoDB('ds062797.mongolab.com:62797/infinity_test', 'luis', 'Espronceda');
        client.connect().then(done).catch(error);
    });

    beforeEach(function(done){
      var servers = [
        {id:1,hostname:'hostname1','state':'BOOTED','site':'TEST','boot':123,'heartbeat':moment().subtract(500,'seconds').unix()},
        {id:2,hostname:'hostname2','state':'ERROR','site':'TEST'},
        {id:3,hostname:'hostname3','state':'ENDED','site':'TEST'},
        {id:4,hostname:'hostname4','state':'BOOTED','site':'TEST' },
        {id:51,hostname:'hostname51','state':'CREATING','site':'TEST'},
        {id:5,hostname:'hostname5','state':'CREATING','site':'TEST' },
        {id:6,hostname:'hostname6','state':'STARTED','site':'TEST','boot':123,'heartbeat':moment().subtract(700,'seconds').unix()},
        {id:7,hostname:'hostname7','state':'STARTED','site':'TEST2','heartbeat':moment().subtract(500,'seconds').unix()},
        {id:8,hostname:'hostname8','state':'ERROR','site':'TEST2'},
        {id:9,hostname:'hostname9','state':'ENDED','site':'TEST2'},
        {id:10,hostname:'hostname10','state':'STARTED','site':'TEST2' },
        {id:11,hostname:'hostname11','state':'STARTED','site':'TEST2'},
        {id:12,hostname:'hostname12','state':'STARTED','site':'TEST2','heartbeat':moment().subtract(700,'seconds').unix()}
      ];

      client.deleteAll().then(function(){
        var promises = [];
        for(var i in servers){
          promises.push(client.insertServer(servers[i]));
        }
        return Promise.all(promises).then(function(){
          var p1 = client.updateServer('hostname5',{'createdTime':moment().subtract(700,'seconds').unix()})
          var p2 = client.updateServer('hostname51',{'createdTime':moment().subtract(200,'seconds').unix()})
          var p3 = client.updateServer('hostname4',{'createdTime':moment().subtract(700,'seconds').unix()})
          return Promise.all([p1,p2,p3])
        })
      }).then(function(){done()})
    })

    afterEach(function(done){
      client.deleteAll().then(done).catch(error);
    });

    it("Delete computer in error state", function (done){
        var obj = {client:connector, db:client, info:config, name: 'TEST'}
        var listServers = function(servers){
            servers.should.have.length(2)
            done()
        }
        Delete.deleteComputersInErrorEndedState(obj).then(listServers)
    })

    it("Delete computer not started", function( done ){
      var obj = {client:connector, db:client, info:config, name: 'TEST'}
      var listServers = function(servers){
          servers.should.have.length(1)
          servers[0].hostname.should.be.exactly('hostname5')
          done()
      }
      Delete.deleteComputersNotStarted(obj).then(listServers)
    })


    xit("Delete computers lost heartbeat", function (done){
        var obj = {client:connector, db:client, info:config, name: 'TEST'}
        var listServers = function(servers){
            servers.should.have.length(1)
            servers[0].hostname.should.be.exactly('hostname6')
            done()
        }
        Delete.deleteComputersLostHeartbeat(obj).then(listServers)
    })


    it("Delete computers no botted", function (done){
        var obj = {client:connector, db:client, info:config, name: 'TEST'}
        var listServers = function(servers){
            servers.should.have.length(1)
            servers[0].hostname.should.be.exactly('hostname5')
            done()
        }
        Delete.deleteComputersNotStarted(obj).then(listServers)
    })

    it("Delete computers booted but no started", function (done){
        var obj = {client:connector, db:client, info:config, name: 'TEST'}
        var listServers = function(servers){
            servers.should.have.length(1)
            servers[0].hostname.should.be.exactly('hostname4')
            done()
        }
        Delete.deleteComputerBootedButNotExecuting(obj).then(listServers)
    })


})
