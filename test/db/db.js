﻿var should = require('should');
var Promise = require("bluebird");
var db = require("../../src/db/db").DB;


function error(err){
    err.should.be.empty;
};

describe("MongoDB test", function () {
    var client;
    this.timeout(1000);

    before(function (done) {
        this.timeout(30000);
        client = new db.MongoDB('ds062797.mongolab.com:62797/infinity_test', 'luis', 'Espronceda', undefined);
        client.connect().then(done).catch(error);
    });

    afterEach(function (done) {
        client.deleteAll().then(done).catch(error);
    });


    it("Insert document", function (done) {
        var server = {
            id: '1',
            hostname: 'server1',
            ip: '192.168.0.100',
            state: 'CREATED'
        };
        client.insertServer(server).then(function(){done()}).catch(error);
    });

    it("List documents", function (done) {
        var server = {
            id: '1',
            hostname: 'server1',
            ip: '192.168.0.100',
            state: 'CREATED',
            site: 'TEST'
        };

       var list = function (servers) {
            servers.length.should.be.exactly(1);
            servers[0].hostname.should.be.exactly('server1');
            servers[0].should.have.property('createdTime');
            done();
        };
        client.insertServer(server).bind(client).then(client.listServers).then(list).catch(error);

    });


    it("Update document", function (done) {
        var server = {
            id: '1',
            hostname: 'server1',
            ip: '192.168.0.100',
            state: 'CREATED',
            site: 'TEST'
        };

        var update = function( ) {
            return client.updateServer({ hostname: 'server1' }, { state: 'UPDATED' });
        }

        var updated = function () {
            return client.listServers({ hostname: 'server1' })
                    .then(function (servers) {
                servers.length.should.be.exactly(1);
                servers[0].hostname.should.be.exactly('server1');
                servers[0].state.should.be.exactly('UPDATED');
                done();
            });
        };
        client.insertServer(server).then(update).then(updated).catch(error);
    });


    it("Delete document", function (done) {
        var server = {
            id: '1',
            hostname: 'server1',
            ip: '192.168.0.100',
            state: 'CREATED'
        };

        var del = function () {
            return client.deleteServer(server);
        }

        client.insertServer(server).then(del).then(done).catch(error);
    });

    it("Delete all servers", function (done) {
        var server = {
            id: '1',
            hostname: 'server1',
            ip: '192.168.0.100',
            state: 'CREATED'
        };

        var check = function () {
            return client.listServers().then(function (servers) {
                servers.length.should.be.exactly(0);
                done();
            });
        };
        client.insertServer(server).bind(client).then(client.deleteAll).then(check).catch(error);
    });


    it("MapReduce servers", function (done) {
        var time = Math.floor(Date.now() / 1000)
        var servers = [{
                id: '1',
                hostname: 'server1',
                ip: '192.168.0.100',
                state: 'CREATED',
                heartbeat: (time - 1000)
            },
        {
                id: '2',
                hostname: 'server2',
                ip: '192.168.0.101',
                state: 'CREATED',
                heartbeat: (time - 500)
            },
        {
                id: '3',
                hostname: 'server3',
                ip: '192.168.0.103',
                state: 'STOPPED'
            }];

        var createAllServers = function() {
            var promises = []
            for (var i in servers) {
                promises.push(client.insertServer(servers[i]));
            }
            return Promise.all(promises);
        }

        var mapReduce = function (){
            var map = function () {
                var time = Math.floor(Date.now() / 1000)
                if (this.state == 'CREATED' && 'heartbeat' in this && (time - this.heartbeat) > 700) {
                    emit(this.hostname, time - this.heartbeat);
                }
            };
            var reduce = function (key, values) { return values; };
            client.mapReduce(map, reduce).then(function (servers) {
                servers.should.have.length(1);
                servers[0]._id.should.be.exactly('server1');
                servers[0].value.should.be.approximately(1000, 100);
                done();
            });
        }
        createAllServers().then(mapReduce).catch(error);
    });
});
