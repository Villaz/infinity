var should = require('should');
var Promise = require("bluebird");
var Infinity = require("../src/infinity").Infinity;
var db = require("../src/db/db").DB;


describe('Infinity tests', function() {
      var infinity = undefined
      var client = undefined

      before(function(done) {
        this.timeout(30000);
        infinity = new Infinity('FZJ')
        infinity.db.host = 'ds062797.mongolab.com:62797/infinity_test'
        infinity.db.connect().then(function(){
          client = new db.MongoDB('ds062797.mongolab.com:62797/infinity_test', 'luis', 'Espronceda');
          client.connect().then(done)
        })

  });

  describe('Update servers', function( ){

    beforeEach(function( done ){
      var servers = [
        {id:1,hostname:'hostname1','state':'STARTED','site':'FZJ'},
        {id:4,hostname:'hostname4','state':'ENDED','site':'FZJ'},
      ]

      promises = []
      for(var i in servers){
        promises.push(client.insertServer(servers[i]));
      }
      Promise.all(promises).then(function(){
        done()
      })
    })


    afterEach(function(done){
      client.deleteAll().then(done).catch(function(){done()})
    });


    it('Delete servers from DB and remove from Site', function(done){
      var servers = [{'id':2,'hostname':'hostname2','state':'STARTED','site':'FZJ'},
                     {'id':3,'hostname':'hostname3','state':'BOOTED','site':'FZJ'}
                    ]
      infinity.client = {compute:{delete:function(){}}}
      infinity.updateServers(servers).then(function(result){
        result.length.should.be.exactly(0)
        done()
      })
    })


    it('Update server from database', function(done){
      var servers = [{'id':1,'hostname':'hostname1','state':'ENDED','site':'FZJ'}]
      infinity.updateServers(servers).then(function(result){
        result.length.should.be.exactly(1)
        result[0].state.should.be.exactly('ENDED')
        done()
      })
    })


    it('Update server from database, Database is ENDED Site is STARTED', function(done){
      var servers = [{'id':4,'hostname':'hostname4','state':'STARTED','site':'FZJ'}]
      infinity.updateServers(servers).then(function(result){
        result.length.should.be.exactly(0)
        done()
      })
    })


    it('Ended server from database', function(done){
      var servers = []
      infinity.updateServers(servers).then(function(servs){
        servs.length.should.be.exactly(0)
        done()
      })
    })
  })
})
