﻿/**
    * Class Azure, provides an interface to use Azure
    *
    * @class Azure
    *
    */
declare class Azure {
    subscriptionId: string;
    baseUrl: string;
    cert: string;
    key: string;
    x_ms_version: string;
    location: AzureLocation;
    service: Service;
    vm: VM;
    configurationSet: ConfigurationSet;
    image: AzureImage;
    storage: AzureStorage;
    serviceCertificate: ServiceCertificate;
    /**
    * Constructor
    * @method constructor
    * @param {Object} params params
    * @param {string} params.subscriptionId Subscription Id
    * @param {string} params.cert Certificate to connect to azure
    * @param {string} params.key Privatekey to authenticate in azure
    */
    constructor(subscriptionId: string, cert: string, key: string);
}    


/**
    * Class Image, provides method to manage images
    *
    * @class Image
    *
    */
    declare class AzureImage {
        url: string;
        azure: Azure;
        
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: Azure);
        
        /**
        *List all images
        *@method list
        */
        list(): any;
}

    /**
    *Set a List of configuration sets to add to Deployment/Role/VM
    *@class ConfigurationSets
    */
    declare class ConfigurationSets {
        configurationSets: ConfigurationSet[];
        /**
        * Constructor
        * @method constructor
        */
        constructor();
        /**
        *Adds new ConfigurationSet
        *@method push
        *@param {ConfigurationSet} configurationSet
        */
        push(configurationSet: ConfigurationSet): void;
        /**
        *Returns and ConfigurationSets XML representation
        *@method toXML
        */
        toXML(): string;
    }

    declare class ConfigurationSet {
        toXML(): string;
    }

    interface ILinuxConfigurationSet {
        hostname: string;
        username: string;
        password: string;
        disableSshPasswordAuthentication?: boolean;
        ssh?: any;
        customData?: string;
        computerName?: string;
        automaticUpdates?: boolean;
        timeZone?: string;
    }

    /**
    *Class LinuxConfigurationSet
    *@class LinuxConfigurationSet
    */
    declare class LinuxConfigurationSet extends ConfigurationSet {
        private params;
        /**
        * Constructor
        * @method constructor
        * @param {ILinuxconfigurationSet} params
        */
        constructor(params: ILinuxConfigurationSet);
        /**
        *Returns and ConfigurationSets XML representation
        *@method toXML
        */
        toXML(): string;
    }

    interface IOSVirtualDisk {
        hostCaching?: string;
        diskLabel?: string;
        diskName?: string;
        mediaLink?: string;
        sourceImageName?: string;
        os?: string;
        resizedSizeInGB?: Number;
        remoteSourceImageLink?: string;
    }
    
    declare class OSVirtualDisk {
        private params;
        constructor(params: IOSVirtualDisk);
        toXML(): string;
    }

    declare class OperationStatus {
        url: string;
        azure: any;
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: any, request_id: string);
        getStatus(): any;
        waitUntilSuccess(): any;
    }

    /**
    * Class Location, provides method to manage locations
    *
    * @class Location
    *
    */
    declare class AzureLocation {
        azure: any;
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: Azure);
        /**
        *Returns the list of all locations
        *@method list
        */
        list(): any;
        /**
        * Create a new location
        *@method create
        *@param {string} name Name of the location
        *@param {string} label Label of the location
        *@param {string} location Geographic location where will be created
        *@param {string} [description] Description of the location
        */
        create(name: string, label: string, location: string, description?: string): any;
        /**
        * Deletes a location
        *@method delete
        *@param {string} name Name of the location
        */
        delete(name: string): any;
        /**
        * Returns the properties of a location
        *@method properties
        *@param {string} name Name of the location
        */
        properties(name: string): any;
        /**
        *Updates a location
        *@method update
        *@param {string} name Name of the location
        *@param {string} [label] Label of the location
        *@param {string} [description] Description of the location
        */
        update(name: string, label?: string, description?: string): any;
    }


    interface IService {
        name: string;
        label: string;
        description?: string;
        location?: string;
        affinityGroup?: string;
        extendedProperties?: any;
        reverse_dns_fqdn?: string;
    }
    /**
    * Class Service, provides method to manage services
    *
    *@class Service
    *
    */
    declare class Service {
        url: string;
        azure: Azure;
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: Azure);
        /**
        *Lists all services
        */
        list(): any;
        /**
        *Creates a new Service. At least location or affinityGroup must be expecified.
        *@method create
        *@param {string} name Name of the service
        *@param {string} label Label of the service
        *@param {string} [description] Description of the service
        *@param {string} [location] Location of the service
        *@param {string} [affinityGroup] Affinity group of the service
        *@param {string} [extendedProperties] Extended properties of the service
        *@param {string} [reverse_dns_fqdn] Specifies the DNS address to which the IP address of the cloud service resolves when queried using a reverse DNS query.
        */
        create(params: IService): any;
        /**
        * Deletes one server, if deleteDisks is true, the disks attached to the server will be deleted.
        *@param {string} name Service name.
        *@param {boolean} [deleteDisks] Set if disks should be deleted.
        */
        delete(name: string, deleteDisks?: boolean): any;
        /**
        *Checks Cloud Service Name Availability
        *@method checkNameAvailability
        *@param {string} name Name of the service
        */
        checkNameAvailability(name: string): any;
    }


    interface IServiceCerticate {
        serviceName: string;
        data: string;
        format: string;
        password?: string;
    }
    declare class ServiceCertificate {
        azure: Azure;
        constructor(azure: Azure);
        list(serviceName: any): any;
        create(params: IServiceCerticate): any;
    }

    interface IKey {
        type: string;
        fingerprint: string;
        path: string;
    }
    interface ISSH {
        publicKeys?: AzureKey[];
        keyPairs: AzureKey[];
    }

    declare class AzureKey {
        params: IKey;
        constructor(params: IKey);
        /**
        *Returns the Key in XML format
        */
        toXML(): string;
        static generateCertificateFingerprint(path: any): any;
    }

    declare class SSH {
        params: ISSH;
        constructor(params: ISSH);
        toXML(): string;
    }

    interface IStorage {
        serviceName: string;
        description?: string;
        affinityGroup?: string;
        location?: string;
        geoReplication?: boolean;
        secondaryReadEnabled?: boolean;
        accountType: string;
    }
    /**
    *Class Storage
    *@class Storage
    */
    declare class AzureStorage {
        url: string;
        azure: Azure;
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: Azure);
        /**
        *Lists all storages in the susbcription
        *@method list
        */
        list(): any;
        /**
        *Creates a new storage
        *@method create
        */
        create(params: IStorage): any;
    }

    interface IRole {
        name: string;
        mediaLocation?: string;
        configurationSets?: ConfigurationSets;
        resourceExtensionReferences?: any;
        vmImageName?: string;
        availabilitySetName?: string;
        dataVirtualHardDisks?: any;
        oSVirtualHardDisk?: OSVirtualDisk;
        roleSize?: any;
        provisionGuestAgent?: boolean;
        vmImageInput?: any;
    }
    /**
    *Class Role
    *@class Role
    */
    declare class Role {
        url: string;
        azure: Azure;
        private _params;
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: Azure);
        /**
        *Set params value
        *@method params
        *@param {IRole} params
        */
        params: IRole;
        /**
        *Get the name of the role
        *@return {string} Name
        */
        name: string;
        /**
        *Creates a new Role
        *@method create
        *@param {string} serviceName
        *@param {IRole} params Role properties
        */
        create(serviceName: string, params?: IRole): any;
        /**
        * List all Roles/VMs in one service
        *@method list
        *@param {string} serviceName
        */
        list(serviceName: string): any;
        /**
        *Returns an XML representation
        *@method toXML
        */
        toXML(): string;
    }
    /**
    *Class VM
    *@class VM
    */
    declare class VM extends Role {
        /**
        * Constructor
        * @method constructor
        * @param {Azure} azure Azure object
        */
        constructor(azure: Azure);
        create(serviceName: any, role: Role): any;
    }

