declare class Compute {
    public id: string;
    public hostname: string;
    public cores: number;
    public memory: number;
    public state: string;
    public disk: any;
    public ips: string[];
    public site: string;
    constructor(id: string);
}
export declare class MachineConfig {
}
export declare class MachineImage {
}
export interface Template {
    id: any;
    machineConfig: any;
    machineImage: any;
    credentials?: any;
}
export declare class MachineTemplate {
    public id: string;
    public resource: string;
    public machine_config: any;
    public machine_image: any;
    public credentials: any;
    public user_data: any;
    public network: any;
    constructor(id: string);
    constructor(id: Template);
    public json(): {
        'resourceURI': string;
        'machineConfig': {
            'href': any;
            'resourceURI': string;
        };
        'machineImage': {
            'href': any;
            'resourceURI': string;
        };
        "networkInterfaces": {}[];
    };
}
